﻿using BankSystem.Database.Configurations;
using BankSystem.Database.Interceptors;
using Microsoft.EntityFrameworkCore;

namespace BankSystem.Database
{
    public class BankSystemDatabase : DbContext
    {
        public BankSystemDatabase(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(BaseConfiguration<>).Assembly);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.AddInterceptors(new SoftDeleteInterceptor())
                          .AddInterceptors(new AuditableInterceptor())
                          .AddInterceptors(new OptimisticLockInterceptor());
        }


    }
}
