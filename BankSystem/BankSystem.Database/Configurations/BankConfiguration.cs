﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    public class BankConfiguration : BaseConfiguration<Bank>
    {
        public override void Configure(EntityTypeBuilder<Bank> builder)
        {
            builder.Navigation(x => x.Products).AutoInclude();
            builder.Navigation(x => x.Discounts).AutoInclude();
            builder.Navigation(x => x.Offices).AutoInclude();
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(x => x.Products).WithOne();
            builder.HasMany(x => x.Discounts).WithOne();
            builder.HasMany(x => x.Offices).WithOne();
            base.Configure(builder);
        }
    }
}
