﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Configurations
{
    public class BaseConfiguration<T> : IEntityTypeConfiguration<T> where T : class
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            if (typeof(T).GetInterfaces().Contains(typeof(IEntity)))
            {
                builder.HasKey(x => ((IEntity)x).Id);
            }
            if (typeof(T).GetInterfaces().Contains(typeof(IAuditable)))
            {
                builder.Property(x => ((IAuditable)x).CreatedOn).IsRequired();
                builder.Property(x => ((IAuditable)x).UpdatedOn).IsRequired();
            }
            if (typeof(T).GetInterfaces().Contains(typeof(ISoftDeletable)))
            {
                builder.Property(x => ((ISoftDeletable)x).IsDeleted).IsRequired().HasDefaultValue(false);
                builder.HasQueryFilter(x => !((ISoftDeletable)x).IsDeleted);
            }
           
            if (typeof(T).GetInterfaces().Contains(typeof(IOptimisticLock)))
            {
                builder.Property(x => ((IOptimisticLock)x).TimeStamp)
                    .HasColumnName("xmin")
                    .HasColumnType("xid")
                    .ValueGeneratedOnAddOrUpdate()
                    .IsConcurrencyToken(); ;
            }

            if (typeof(T).GetInterfaces().Contains(typeof(IHasDomainEvents)))
            {
                builder.Ignore(x => ((IHasDomainEvents)x).Events);
            }
        }
    }
}
