﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    public class DiscountConfiguration : BaseConfiguration<Discount>
    {
        public override void Configure(EntityTypeBuilder<Discount> builder)
        {
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            builder.Property(x => x.DiscountRate).HasDefaultValue(0).IsRequired();
            base.Configure(builder);
        }
    }
}
