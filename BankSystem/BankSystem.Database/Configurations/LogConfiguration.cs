﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    internal class LogConfiguration : BaseConfiguration<Log>
    {
        public override void Configure(EntityTypeBuilder<Log> builder)
        {
            builder.HasIndex(x => x.EventId);
            builder.Property(x => x.OccuredOn).IsRequired();
            builder.Property(x => x.MessageType).IsRequired().HasMaxLength(255);
            builder.Property(x => x.Message).HasColumnType("jsonb").IsRequired();
            builder.Property(x => x.ActionType).HasColumnType("text").IsRequired();

            base.Configure(builder);
        }
    }
}
