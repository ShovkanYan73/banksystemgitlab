﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    public class ManagerConfiguration : BaseConfiguration<Manager>
    {
        public override void Configure(EntityTypeBuilder<Manager> builder)
        {
            builder.OwnsOne(x => x.FullName, _ =>
            {
                _.Property(x => x.Name).HasMaxLength(255).IsRequired();
                _.Property(x => x.Surname).HasMaxLength(255).IsRequired();
                _.Property(x => x.Patronymic).HasMaxLength(255).IsRequired(false);
            });
            builder.Property(x => x.Salary).IsRequired();
            base.Configure(builder);
        }
    }
}
