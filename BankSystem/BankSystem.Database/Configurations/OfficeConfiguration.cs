﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    public class OfficeConfiguration : BaseConfiguration<Office>
    {

        public override void Configure(EntityTypeBuilder<Office> builder)
        {
            builder.Navigation(x => x.Managers).AutoInclude();
            builder.Property(x => x.OpenTime).IsRequired();
            builder.Property(x => x.ClosedTime).IsRequired();
            builder.OwnsOne(x => x.Address, _ =>
            {
                _.Property(x => x.Country).HasMaxLength(100).IsRequired(false);
                _.Property(x => x.CountryFiasId).IsRequired(false);
                _.Property(x => x.Region).HasMaxLength(100).IsRequired(false);
                _.Property(x => x.RegionFiasId).IsRequired(false);
                _.Property(x => x.City).HasMaxLength(100).IsRequired(false);
                _.Property(x => x.CityFiasId).IsRequired(false);
                _.Property(x => x.District).HasMaxLength(100).IsRequired(false);
                _.Property(x => x.DistrictFiasId).IsRequired(false);
                _.Property(x => x.Street).HasMaxLength(100).IsRequired(false);
                _.Property(x => x.StreetFiasId).IsRequired(false);
                _.Property(x => x.House).HasMaxLength(100).IsRequired(false);
                _.Property(x => x.HouseFiasId).IsRequired(false);
            });
            builder.HasMany(x => x.Managers).WithOne();
            base.Configure(builder);
        }
    }
}
