﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Configurations
{
    public class OutboxConfiguration : BaseConfiguration<Outbox>
    {
        public override void Configure(EntityTypeBuilder<Outbox> builder)
        {
            builder.HasIndex(x => x.EventId);
            builder.Property(x => x.OccuredOn).IsRequired();
            builder.Property(x => x.Message).HasColumnType("jsonb").IsRequired();
            builder.Property(x => x.MessageType).IsRequired().HasMaxLength(255);
            builder.Property(x => x.AssemblyName).IsRequired().HasMaxLength(255);
            builder.Property(x => x.OutboxStatus).HasColumnType("text").IsRequired();

            base.Configure(builder);
        }
    }
}
