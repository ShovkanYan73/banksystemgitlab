﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    public class ProductConfiguration : BaseConfiguration<Product>
    {

        public override void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Navigation(x => x.Subproducts).AutoInclude();
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            builder.HasMany(x => x.Subproducts).WithOne();
            base.Configure(builder);
        }
    }
}
