﻿using BankSystem.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BankSystem.Database.Configurations
{
    public class SubproductConfiguration : BaseConfiguration<Subproduct>
    {
        public override void Configure(EntityTypeBuilder<Subproduct> builder)
        {
            builder.Property(x => x.Rate).HasDefaultValue(0).IsRequired();
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            base.Configure(builder);
        }
    }
}
