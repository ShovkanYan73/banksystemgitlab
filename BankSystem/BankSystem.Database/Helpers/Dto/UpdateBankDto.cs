﻿using BankSystem.Database.Models;

namespace BankSystem.Database.Helpers.Dto
{
    /// <summary>
    /// Модель для обновления банка в БД.
    /// </summary>
    public record UpdateBankDto
    {
        /// <summary>
        /// Наименование банка.
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// Список офисов банка.
        /// </summary>
        public List<Office>? Offices { get; init; }

        /// <summary>
        /// Список продукции банка.
        /// </summary>
        public List<Product>? Products { get; init; }

        /// <summary>
        /// Список скидок банка.
        /// </summary>
        public List<Discount>? Discounts { get; init; }
    }
}
