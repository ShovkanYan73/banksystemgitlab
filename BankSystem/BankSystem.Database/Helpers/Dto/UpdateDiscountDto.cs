﻿namespace BankSystem.Database.Helpers.Dto
{
    /// <summary>
    /// Модель для обновления продукта в БД.
    /// </summary>
    public record UpdateDiscountDto
    {
        /// <summary>
        /// Идентификатор скидки.
        /// </summary>
        public Guid DiscountId { get; set; }

        /// <summary>
        /// Наименование скидки.
        /// </summary>
        public string? Name { get; init; }
        /// <summary>
        /// Процент скидки.
        /// </summary>
        public decimal? DiscountRate { get; init; }
    }
}
