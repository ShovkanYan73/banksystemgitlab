﻿using BankSystem.Database.Models;

namespace BankSystem.Database.Helpers.Dto
{
    /// <summary>
    /// Модель для обновления продукта в БД.
    /// </summary>
    public record UpdateProductDto
    {
        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// Наименование продукта.
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// Список субпродуктов продукта.
        /// </summary>
        public List<Subproduct>? Subproducts { get; init; }
    }
}
