﻿namespace BankSystem.Database.Helpers.Dto
{
    /// <summary>
    /// Модель данных для обновления субпродукта в БД.
    /// </summary>
    public record UpdateSubproductDto
    {
        /// <summary>
        /// Идентификатор субпродукта.
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// Идентификатор субпродукта.
        /// </summary>
        public Guid SubproductId { get; set; }

        /// <summary>
        /// Наименование субпродукта.
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// Процентная ставка субпродукта.
        /// </summary>
        public decimal? Rate { get; init; }
    }
}
