﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Helpers.Enums
{
    public enum ActionType
    {
        Default,
        Updated,
        Deleted
    }
}
