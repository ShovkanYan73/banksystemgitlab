﻿namespace BankSystem.Database.Helpers.Enums
{
    public enum OutboxStatus
    {
        Default,
        Unsended,
        Sended,
        SendingFailed
    }
}
