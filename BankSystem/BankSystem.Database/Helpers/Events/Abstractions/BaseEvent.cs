﻿using System.Text.Json.Serialization;

namespace BankSystem.Database.Helpers.Events.Abstractions
{
    public abstract record BaseEvent
    {
        [JsonIgnore]
        public Guid EventId { get; set; }

        [JsonIgnore]
        public DateTime OccuredOn { get; set; }
    }
}
