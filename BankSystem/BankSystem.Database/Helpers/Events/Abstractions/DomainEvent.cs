﻿using MediatR;

namespace BankSystem.Database.Helpers.Events.Abstractions
{
    public abstract record DomainEvent : BaseEvent, IRequest
    {
    }
}
