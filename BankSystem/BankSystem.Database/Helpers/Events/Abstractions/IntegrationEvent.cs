﻿using Confluent.Kafka;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Helpers.Events.Abstractions
{
    public record IntegrationEvent : BaseEvent, IRequest
    {
    }
}
