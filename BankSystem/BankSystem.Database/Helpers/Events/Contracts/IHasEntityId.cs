﻿namespace BankSystem.Database.Helpers.Events.Contracts
{
    /// <summary>
    /// Контракт на идентификатор сущности для ивентов.
    /// </summary>
    public interface IHasEntityId
    {
        /// <summary>
        /// Идентификатор сущности.
        /// </summary>
        public Guid EntityId { get; set; }
    }
}
