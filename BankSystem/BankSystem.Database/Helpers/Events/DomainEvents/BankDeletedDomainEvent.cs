﻿using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Helpers.Events.DomainEvents
{
    public record BankDeletedDomainEvent : DomainEvent
    {
        public Guid BankId { get; set; }
    }
}
