﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Helpers.Events.DomainEvents
{
    public record BankUpdatedDomainEvent : DomainEvent
    {
        public Guid BankId { get; set; }

        public UpdateBankDto UpdateBankDto { get; set; }
    }
}
