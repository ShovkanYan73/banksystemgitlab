﻿using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Helpers.Events.DomainEvents
{
    public record DiscountDeletedDomainEvent : DomainEvent
    {
        public Guid BankId { get; set; }

        public Guid DiscountId { get; set; }
    }
}
