﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Helpers.Events.DomainEvents
{
    public record DiscountUpdatedDomainEvent : DomainEvent
    {
        public Guid BankId { get; set; }

        public Guid DiscountId { get; set; }

        public UpdateDiscountDto UpdateDiscountDto { get; set; }
    }
}
