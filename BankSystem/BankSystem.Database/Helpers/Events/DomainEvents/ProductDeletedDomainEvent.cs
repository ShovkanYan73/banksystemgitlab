﻿using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Helpers.Events.DomainEvents
{
    public record ProductDeletedDomainEvent : DomainEvent
    {
        public Guid BankId { get; set; }

        public Guid ProductId { get; set; }
    }
}
