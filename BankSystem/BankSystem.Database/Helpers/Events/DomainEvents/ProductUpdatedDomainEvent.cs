﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Helpers.Events.DomainEvents
{
    public record ProductUpdatedDomainEvent : DomainEvent
    {
        public Guid BankId { get; set; }

        public Guid ProductId { get; set; }

        public UpdateProductDto UpdateProductDto { get; set; }
    }
}
