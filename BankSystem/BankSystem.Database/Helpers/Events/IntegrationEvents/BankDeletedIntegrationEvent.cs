﻿using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.Contracts;

namespace BankSystem.Database.Helpers.Events.IntegrationEvents
{
    public record BankDeletedIntegrationEvent : IntegrationEvent, IHasEntityId
    {
        public Guid EntityId { get; set; }
    }
}
