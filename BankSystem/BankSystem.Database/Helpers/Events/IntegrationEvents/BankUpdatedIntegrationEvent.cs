﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.Contracts;

namespace BankSystem.Database.Helpers.Events.IntegrationEvents
{
    public record BankUpdatedIntegrationEvent : IntegrationEvent, IHasEntityId
    {
        public Guid EntityId { get; set; }

        public UpdateBankDto UpdateBankDto { get; set; }
    }
}
