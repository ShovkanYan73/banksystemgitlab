﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.Contracts;

namespace BankSystem.Database.Helpers.Events.IntegrationEvents
{
    public record DiscountUpdatedIntegrationEvent : IntegrationEvent, IHasEntityId
    {
        public Guid BankId { get; set; }

        public Guid EntityId { get; set; }

        public UpdateDiscountDto UpdateDiscountDto { get; set; }
    }
}
