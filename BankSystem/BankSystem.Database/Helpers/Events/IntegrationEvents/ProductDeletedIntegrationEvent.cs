﻿using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.Contracts;

namespace BankSystem.Database.Helpers.Events.IntegrationEvents
{
    public  record ProductDeletedIntegrationEvent : IntegrationEvent, IHasEntityId
    {
        public Guid BankId { get; set; }

        public Guid EntityId { get; set; }
    }
}
