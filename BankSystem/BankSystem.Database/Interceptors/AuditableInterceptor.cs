﻿using BankSystem.Database.Models.Contracts;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Interceptors
{
    internal class AuditableInterceptor : SaveChangesInterceptor
    {
        public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result, CancellationToken cancellationToken = default)
        {
            DateTime date = DateTime.Now.ToUniversalTime();
            foreach (var entityToAdd in eventData.Context.ChangeTracker.Entries().Where(x => x.State == EntityState.Added && x.Entity.GetType().GetInterfaces().Contains(typeof(IAuditable))))
            {
                entityToAdd.Property(nameof(IAuditable.CreatedOn)).CurrentValue = date;
                entityToAdd.Property(nameof(IAuditable.UpdatedOn)).CurrentValue = date;
            }

            foreach (var entityToModify in eventData.Context.ChangeTracker.Entries().Where(x => x.State == EntityState.Modified && x.Entity.GetType().GetInterfaces().Contains(typeof(IAuditable))))
            {
                entityToModify.Property(nameof(IAuditable.UpdatedOn)).CurrentValue = date;
            }

            return base.SavingChangesAsync(eventData, result, cancellationToken);
        }
    }

}
