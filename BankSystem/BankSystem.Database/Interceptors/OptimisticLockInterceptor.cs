﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Interceptors
{
    internal class OptimisticLockInterceptor : SaveChangesInterceptor
    {
        public override Task SaveChangesFailedAsync(DbContextErrorEventData eventData, CancellationToken cancellationToken = default)
        {
            if (eventData.Exception is DBConcurrencyException)
            {
                throw new Exception("Дананя сущность уже обновлена в базе", eventData.Exception);
            }
            throw eventData.Exception;
        }
    }

}
