﻿using BankSystem.Database.Models.Contracts;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Interceptors
{
    internal class SoftDeleteInterceptor : SaveChangesInterceptor
    {
        public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result, CancellationToken cancellationToken = default)
        {
            foreach (var entityToDelete in eventData.Context.ChangeTracker.Entries().Where(x => x.State == EntityState.Deleted && x.Entity.GetType().GetInterfaces().Contains(typeof(ISoftDeletable))))
            {
                entityToDelete.Property(nameof(ISoftDeletable.IsDeleted)).CurrentValue = true;
                entityToDelete.State = EntityState.Modified;
            }

            return base.SavingChangesAsync(eventData, result, cancellationToken);
        }
    }

}
