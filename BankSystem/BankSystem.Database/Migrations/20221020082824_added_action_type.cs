﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BankSystem.Database.Migrations
{
    public partial class added_action_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActionType",
                table: "Log",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionType",
                table: "Log");
        }
    }
}
