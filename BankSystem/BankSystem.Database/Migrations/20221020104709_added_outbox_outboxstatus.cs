﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BankSystem.Database.Migrations
{
    public partial class added_outbox_outboxstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OutboxStatus",
                table: "Outbox",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OutboxStatus",
                table: "Outbox");
        }
    }
}
