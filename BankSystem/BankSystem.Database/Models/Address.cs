﻿namespace BankSystem.Database.Models
{
    public record Address
    {
        public Address(string? country = default,
                       Guid? countryFiasId = default,
                       string? region = default,
                       Guid? regionFiasId = default,
                       string? city = default,
                       Guid? cityFiasId = default,
                       string? district = default,
                       Guid? districtFiasId = default,
                       string? street = default,
                       Guid? streetFiasId = default,   
                       string? house = default,
                       Guid? houseFiasId = default)
        {
            Country = country;
            CountryFiasId = countryFiasId;
            Region = region;
            RegionFiasId = regionFiasId;
            City = city;
            CityFiasId = cityFiasId;
            District = district;
            DistrictFiasId = districtFiasId;
            Street = street;
            StreetFiasId = streetFiasId;
            House = house;
            HouseFiasId = houseFiasId;
        }

        private Address()
        {
        }


        public string? Country { get; internal init; }
        public Guid? CountryFiasId { get; internal init; }
        public string? Region { get; internal init; }
        public Guid? RegionFiasId { get; internal init; }
        public string? City { get; internal init; }
        public Guid? CityFiasId { get; internal init; }
        public string? District { get; internal init; }
        public Guid? DistrictFiasId { get; internal init; }
        public string? Street { get; internal init; }
        public Guid? StreetFiasId { get; internal init; }
        public string? House { get; internal init; }
        public Guid? HouseFiasId { get; internal init; }
    }
}
