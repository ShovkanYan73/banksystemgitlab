﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Helpers.Events.IntegrationEvents;
using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Models
{
    public class Bank : IHasDomainEvents, IEntity, IOptimisticLock, ISoftDeletable, IAuditable, IAggregate
    {
        private Bank()
        {
        }

        public Bank(string name)
        {
            Name = name;
        }

        public void UpdateBank(UpdateBankDto dto)
        {
            Name = dto.Name ?? Name;
            Offices = dto.Offices ?? Offices;
            Products = dto.Products ?? Products;
            Discounts = dto.Discounts ?? Discounts;

            Events.Add(new BankUpdatedDomainEvent
            {
                BankId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime(),
                UpdateBankDto = dto
            });

            Events.Add(new BankUpdatedIntegrationEvent
            {
                EntityId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime(),
                UpdateBankDto = dto
            });
        }

        public void DeleteBank()
        {
            IsDeleted = true;

            Events.Add(new BankDeletedDomainEvent
            {
                BankId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime()
            });
            
            Events.Add(new BankDeletedIntegrationEvent
            {
                EntityId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime()
            });
        }

        public void AddProduct(Product product)
        {
            Products.Add(product);
        }

        public void UpdateProduct(UpdateProductDto dto)
        {
            var product = Products.FirstOrDefault(x => x.Id == dto.ProductId);

            if (product == null)
            {
                throw new Exception("Данного продукта не существует у этого банка.");
            }

            product.Update(dto, Id);
        }

        public void DeleteProduct(Guid productId)
        {
            var product = Products.FirstOrDefault(x => x.Id == productId);

            if (product == null)
            {
                throw new Exception("Данного продукта не существует у этого банка.");
            }

            product.Delete(Id);
        }

        public void AddSubproduct(Guid productId, Subproduct subproduct)
        {
            var product = Products.FirstOrDefault(x => x.Id == productId);

            if (product == null)
            {
                throw new Exception("Данного продукта не существует у этого банка.");
            }

            product.AddSubproduct(subproduct);
        }

        public void UpdateSubproduct(UpdateSubproductDto dto)
        {
            var product = Products.FirstOrDefault(x => x.Id == dto.ProductId);

            if (product == null)
            {
                throw new Exception("Данного продукта не существует у этого банка.");
            }

            product.UpdateSubproduct(dto);
        }

        public void DeleteSubproduct(Guid productId, Guid subproductId)
        {
            var product = Products.FirstOrDefault(x => x.Id == productId);

            if (product == null)
            {
                throw new Exception("Данного продукта не существует у этого банка.");
            }

            product.DeleteSubproduct(subproductId);
        }

        public void AddDiscount(Discount discount)
        {
            Discounts.Add(discount);
        }

        public void UpdateDiscount(UpdateDiscountDto dto)
        {
            var discount = Discounts.FirstOrDefault(x => x.Id == dto.DiscountId);

            if (discount == null)
            {
                throw new Exception("Данной скидки не существует у этого банка.");
            }

            discount.Update(dto, Id);
        }

        public void DeleteDiscount(Guid discountId)
        {
            var discount = Discounts.FirstOrDefault(x => x.Id == discountId);

            if (discount == null)
            {
                throw new Exception("Данной скидки не существует у этого банка.");
            }

            discount.Delete(Id);
        }

        public Guid Id { get; internal set; }

        public string Name { get; internal set; }

        public virtual List<Office> Offices { get; internal set; } = new List<Office>();

        public virtual List<Product> Products { get; internal set; } = new List<Product>();

        public virtual List<Discount> Discounts { get; internal set; } = new List<Discount>();

        public uint TimeStamp { get; internal set; }

        public bool IsDeleted { get; internal set; }

        public DateTime CreatedOn { get; internal set; }

        public DateTime UpdatedOn { get; internal set; }

        public List<BaseEvent> Events { get; set; } = new List<BaseEvent>();
    }
}
