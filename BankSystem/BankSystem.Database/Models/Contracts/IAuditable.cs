﻿namespace BankSystem.Database.Models.Contracts
{
    /// <summary>
    /// Contract for entities with creation and update date
    /// </summary>
    public interface IAuditable
    {
        /// <summary>
        /// Date of creation
        /// </summary>
        public DateTime CreatedOn { get; }

        /// <summary>
        /// Date of update
        /// </summary>
        public DateTime UpdatedOn { get; }
    }
}
