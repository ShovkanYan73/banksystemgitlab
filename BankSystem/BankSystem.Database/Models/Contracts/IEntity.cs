﻿namespace BankSystem.Database.Models.Contracts
{
    /// <summary>
    /// Contract for entities with id
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Entity id
        /// </summary>
        public Guid Id { get; }
    }
}
