﻿using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Database.Models.Contracts
{
    public interface IHasDomainEvents
    {
        public List<BaseEvent> Events { get; set; }
    }
}
