﻿namespace BankSystem.Database.Models.Contracts
{
    /// <summary>
    /// Contract for entities with number of changes
    /// </summary>
    public interface IOptimisticLock
    {
        public uint TimeStamp { get; }
    }
}
