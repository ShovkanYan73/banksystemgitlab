﻿namespace BankSystem.Database.Models.Contracts
{
    /// <summary>
    /// Contract for entites for soft delete
    /// </summary>
    public interface ISoftDeletable
    {
        public bool IsDeleted { get; }
    }
}
