﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Helpers.Events.IntegrationEvents;
using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Models
{
    public class Discount : IHasDomainEvents, IEntity, IOptimisticLock, ISoftDeletable, IAuditable
    {
        private Discount()
        {
        }

        public Discount(string name,
                        decimal discountRate)
        {
            Name = name;
            DiscountRate = discountRate;
        }

        internal void Delete(Guid bankId)
        {
            IsDeleted = true;

            Events.Add(new DiscountDeletedDomainEvent
            {
                BankId = bankId,
                DiscountId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime()
            });

            Events.Add(new DiscountDeletedIntegrationEvent
            {
                BankId = bankId,
                EntityId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime()
            });
        }

        internal void Update(UpdateDiscountDto dto, Guid bankId)
        {
            Name = dto.Name ?? Name;
            DiscountRate = dto.DiscountRate ?? DiscountRate;


            Events.Add(new DiscountUpdatedDomainEvent
            {
                BankId = bankId,
                DiscountId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime(),
                UpdateDiscountDto = dto
            });

            Events.Add(new DiscountUpdatedIntegrationEvent
            {
                BankId = bankId,
                EntityId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime(),
                UpdateDiscountDto = dto
            });
        }

        public Guid Id { get; private set; }


        public string Name { get; private set; }


        public decimal DiscountRate { get; private set; }


        public uint TimeStamp { get; private set; }


        public bool IsDeleted { get; private set; }


        public DateTime CreatedOn { get; private set; }


        public DateTime UpdatedOn { get; private set; }


        public List<BaseEvent> Events { get; set; } = new List<BaseEvent>();

    }
}
