﻿namespace BankSystem.Database.Models
{
    public record FullName
    {
        private FullName()
        {
        }

        public FullName(string name,
                        string surname,
                        string? patronymic = default)
        {
            Name = name;
            Surname = surname;
            Patronymic = patronymic;
        }

        public string Name { get; internal set; }

        public string Surname { get; internal set; }

        public string? Patronymic { get; internal set; }
    }
}
