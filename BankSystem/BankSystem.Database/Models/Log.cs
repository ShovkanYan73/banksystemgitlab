﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events;
using BankSystem.Database.Models.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Models
{
    public class Log : IEntity, IOptimisticLock, ISoftDeletable, IAuditable, IAggregate
    {
        private Log()
        {
        }

        public Log(Guid eventId, DateTime occuredOn, string message, string messageType, ActionType actionType)
        {
            EventId = eventId;
            OccuredOn = occuredOn;
            Message = message;
            MessageType = messageType;
            ActionType = actionType;
        }

        public Guid Id { get; private set; }

        public Guid EventId { get; private set; }

        public DateTime OccuredOn { get; private set; }

        public string Message { get; internal set; } 

        public string MessageType { get; private set; }

        public ActionType ActionType { get; private set; }
        
        public uint TimeStamp { get; private set; }

        public bool IsDeleted { get; private set; }

        public DateTime CreatedOn { get; private set; }

        public DateTime UpdatedOn { get; private set; }
    }
}
