﻿using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Models
{
    public class Manager : IEntity, IOptimisticLock, ISoftDeletable, IAuditable
    {
        private Manager()
        {
        }

        public Manager(FullName fullName,
                       decimal salary)
        {
            FullName = fullName;
            Salary = salary;
        }

        public Guid Id { get; internal set; }

        public FullName FullName { get; internal set; }        
        
        public decimal Salary { get; internal set; }
        
        public DateTime CreatedOn { get; internal set; }
        
        public DateTime UpdatedOn { get; internal set; }

        public bool IsDeleted { get; internal set; }

        public uint TimeStamp { get; internal set; }

    }
}
