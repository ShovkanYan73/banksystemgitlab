﻿using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Models
{
    public class Office : IEntity, ISoftDeletable
    {
        private Office()
        {
        }

        public Office(DateTime openTime,
                      DateTime closedTime,
                      Address address)
        {
            OpenTime = openTime;
            ClosedTime = closedTime;
            Address = address;
        }


        public Guid Id { get; internal set; }


        public DateTime OpenTime { get; internal set; }


        public DateTime ClosedTime { get; internal set; }


        public Address Address { get; internal set; }


        public List<Manager> Managers { get; internal set; } = new List<Manager>();


        public bool IsDeleted { get; internal set; }
    }
}
