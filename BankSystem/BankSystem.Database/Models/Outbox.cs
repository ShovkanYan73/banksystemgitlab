﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Models
{
    public class Outbox : IEntity, IOptimisticLock, ISoftDeletable, IAuditable, IAggregate
    {
        private Outbox()
        {
        }

        public Outbox(Guid eventId, DateTime occuredOn, string message, string messageType, string assemblyName)
        {
            EventId = eventId;
            OccuredOn = occuredOn;
            Message = message;
            MessageType = messageType;
            AssemblyName = assemblyName;
            OutboxStatus = OutboxStatus.Unsended;
        }

        public void UpdateOutboxStatus(OutboxStatus outboxStatus)
        {
            OutboxStatus = outboxStatus;
        }

        public Guid Id { get; private set; }

        public Guid EventId { get; private set; }

        public DateTime OccuredOn { get; private set; }

        public string Message { get; internal set; }

        public string MessageType { get; private set; }

        public string AssemblyName { get; private set; }

        public OutboxStatus OutboxStatus { get; private set; }

        public uint TimeStamp { get; private set; }

        public bool IsDeleted { get; private set; }

        public DateTime CreatedOn { get; private set; }

        public DateTime UpdatedOn { get; private set; }
    }
}
