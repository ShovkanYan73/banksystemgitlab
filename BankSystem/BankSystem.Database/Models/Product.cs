﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Helpers.Events.IntegrationEvents;
using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Models
{
    public class Product : IHasDomainEvents, IEntity, ISoftDeletable, IAuditable
    {
        private Product()
        {
        }

        public Product(string name)
        {
            Name = name;
        }

        internal void Delete(Guid bankId)
        {
            IsDeleted = true;

            Events.Add(new ProductDeletedDomainEvent
            {
                BankId = bankId,
                ProductId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime()
            });

            Events.Add(new ProductDeletedIntegrationEvent
            {
                BankId = bankId,
                EntityId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime()
            });
        }

        internal void Update(UpdateProductDto dto, Guid bankId)
        {
            Name = dto.Name ?? Name;
            Subproducts = dto.Subproducts ?? Subproducts;

            Events.Add(new ProductUpdatedDomainEvent
            {
                BankId = bankId,
                ProductId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime(),
                UpdateProductDto = dto
            });

            Events.Add(new ProductUpdatedIntegrationEvent
            {
                BankId = bankId,
                EntityId = Id,
                EventId = Guid.NewGuid(),
                OccuredOn = DateTime.Now.ToUniversalTime(),
                UpdateProductDto = dto
            });
        }

        internal void AddSubproduct(Subproduct subproduct)
        {
            Subproducts.Add(subproduct);
        }

        internal void UpdateSubproduct(UpdateSubproductDto dto)
        {
            var subproduct = Subproducts.FirstOrDefault(x => x.Id == dto.SubproductId);

            if (subproduct == null)
            {
                throw new Exception("Данного субпродукта не существует у этого продукта.");
            }

           subproduct.Update(dto);
        }

        internal void DeleteSubproduct(Guid subroductId)
        {
            var subproduct = Subproducts.FirstOrDefault(x => x.Id == subroductId);

            if (subproduct == null)
            {
                throw new Exception("Данного субпродукта не существует у этого продукта.");
            }

            subproduct.Delete();
        }

        public Guid Id { get; private set; }


        public string Name { get; private set; }


        public List<Subproduct> Subproducts { get; private set; } = new List<Subproduct>();


        public DateTime CreatedOn { get; private set; }


        public DateTime UpdatedOn { get; private set; }


        public bool IsDeleted { get; private set; }

        public List<BaseEvent> Events { get; set; } = new List<BaseEvent>();


    }
}
