﻿using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models.Contracts;

namespace BankSystem.Database.Models
{
    public class Subproduct : IEntity, ISoftDeletable, IAuditable
    {
        private Subproduct()
        {
        }

        public Subproduct(string name, 
                          decimal rate)
        {
            Name = name;
            Rate = rate;
        }
        internal void Delete()
        {
            IsDeleted = true;
        }

        internal void Update(UpdateSubproductDto dto)
        {
            Name = dto.Name ?? Name;
            Rate = dto.Rate ?? Rate;
        }

        public Guid Id { get; private set; }


        public string Name { get; private set; }


        public decimal Rate { get; private set; }


        public bool IsDeleted { get; private set; }


        public DateTime CreatedOn { get; private set; }


        public DateTime UpdatedOn { get; private set; }
    }
}
