﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Services.Contracts
{
    /// <summary>
    /// Единый сервис для сохранения результатов работы с БД.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Зафиксировать измененеия в БД.
        /// </summary>
        /// <returns></returns>
        public void Commit();

        /// <summary>
        /// Асинхронно зафиксировать измененеия в БД.
        /// </summary>
        /// <returns></returns>
        public Task CommitAsync();
    }

}
