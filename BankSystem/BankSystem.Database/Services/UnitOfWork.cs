﻿using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Models.Contracts;
using BankSystem.Database.Services.Contracts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem.Database.Services
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BankSystemDatabase _context;
        private readonly IMediator? _mediator;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="UnitOfWork"/>
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mediator"></param>
        public UnitOfWork(BankSystemDatabase context, IMediator? mediator = null)
        {
            _context = context;
            _mediator = mediator;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync()
        {
            if(_mediator != null)
            {
                var baseEvents = _context.ChangeTracker.Entries()
               .Where(x => x.Entity.GetType().GetInterfaces().Contains(typeof(IHasDomainEvents)))
               .SelectMany(x =>
               {
                   var entityWithEvents = (IHasDomainEvents)x.Entity;
                   var events = entityWithEvents.Events;

                   if (events == null || !events.Any())
                   {
                       return Enumerable.Empty<BaseEvent>();
                   }

                   return events;
               }).ToArray();

                foreach (var baseEvent in baseEvents)
                {
                    await _mediator.Send(baseEvent);
                }
            }

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public void Commit()
        {
            if (_mediator != null)
            {
                var baseEvents = _context.ChangeTracker.Entries()
               .Where(x => x.Entity.GetType().GetInterfaces().Contains(typeof(IHasDomainEvents)))
               .SelectMany(x =>
               {
                   var entityWithEvents = (IHasDomainEvents)x.Entity;
                   var events = entityWithEvents.Events;

                   if (events == null || !events.Any())
                   {
                       return Enumerable.Empty<BaseEvent>();
                   }

                   return events;
               }).ToArray();

                foreach (var baseEvent in baseEvents)
                {
                    _mediator.Send(baseEvent);
                }
            }

            _context.SaveChanges();
        }
    }
}
