﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Job.QueryHandlers.GetUnsendedOutboxes;

namespace BankSystem.Job.Automapper.Profiles
{
    public class OutboxProfile : Profile
    {
        public OutboxProfile()
        {
            CreateMap<Outbox, GetUnsendedOutboxesSingleOutbox>();
        }
    }
}
