﻿using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Job.Contracts;

namespace BankSystem.Job.CommandHandlers.UpdateOutbox
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class UpdateOutboxCommandHandler : ICommandHandler<UpdateOutboxRequestDto>
    {
        private readonly ICommandRepository<Outbox> _repository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="UpdateOutboxCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="unitOfWork"></param>
        public UpdateOutboxCommandHandler(ICommandRepository<Outbox> repository,
                                        IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(UpdateOutboxRequestDto model)
        {
            var outbox = await _repository.GetByIdAsync(model.OutboxId);

            if(outbox == null)
            {
                throw new Exception("Обновляемого сообщения об изменениях не существует в БД.");
            }

            outbox.UpdateOutboxStatus(model.OutboxStatus);
            await _unitOfWork.CommitAsync();
        }
    }
}
