﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Job.Contracts;

namespace BankSystem.Job.CommandHandlers.UpdateOutbox
{
    /// <summary>
    /// Модель данных для команды на обновление сообщения об изменениях.
    /// </summary>
    public record UpdateOutboxRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор сообщения об изменениях.
        /// </summary>
        public Guid OutboxId { get; init; }

        /// <summary>
        /// Статус сообщения об изменениях.
        /// </summary>
        public OutboxStatus OutboxStatus { get; init; }
    }
}
