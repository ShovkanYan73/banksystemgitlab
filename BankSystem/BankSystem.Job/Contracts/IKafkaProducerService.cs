﻿using BankSystem.Database.Helpers.Events.Abstractions;

namespace BankSystem.Job.Contracts
{
    /// <summary>
    /// Класс для публикации сообщений в Kafka.
    /// </summary>
    public interface IKafkaProducerService
    {
        /// <summary>
        /// Публикация сообщения.
        /// </summary>
        /// <param name="integrationEvent">Сообщение.</param>
        /// <param name="OutboxId">Идентификатор события.</param>
        /// <returns></returns>
        public Task SendMessage(IntegrationEvent integrationEvent, Guid OutboxId);
    }
}
