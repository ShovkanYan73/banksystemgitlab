﻿using Confluent.Kafka;

namespace BankSystem.Job.Contracts
{
    public interface IProducerFactory
    {

        public IProducer<TKey, TValue> Create<TKey, TValue>(string name);

    }
}
