﻿using BankSystem.Job.Contracts;

namespace BankSystem.Job.QueryHandlers
{
    public record EmptyQueryRequestDto : IQueryDto
    {

    }
}
