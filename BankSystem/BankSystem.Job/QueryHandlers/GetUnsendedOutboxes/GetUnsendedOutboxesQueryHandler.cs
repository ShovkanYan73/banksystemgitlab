﻿using AutoMapper;
using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Job.Contracts;
using Microsoft.EntityFrameworkCore;

namespace BankSystem.Job.QueryHandlers.GetUnsendedOutboxes
{
    public class GetUnsendedOutboxesQueryHandler : IQueryHandler<EmptyQueryRequestDto, GetUnsendedOutboxesResponseDto>
    {
        private readonly IQueryRepository<Outbox> _repository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="GetUnsendedOutboxesQueryHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public GetUnsendedOutboxesQueryHandler(IQueryRepository<Outbox> repository,
                                       IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<GetUnsendedOutboxesResponseDto> Handle(EmptyQueryRequestDto model)
        {
            var outboxes = await _repository.GetQuery().Where(x => x.OutboxStatus == OutboxStatus.Unsended).ToArrayAsync();
            return new GetUnsendedOutboxesResponseDto
            {
                Outboxes = _mapper.Map<IReadOnlyCollection<GetUnsendedOutboxesSingleOutbox>>(outboxes)
            };
        }
    }
}
