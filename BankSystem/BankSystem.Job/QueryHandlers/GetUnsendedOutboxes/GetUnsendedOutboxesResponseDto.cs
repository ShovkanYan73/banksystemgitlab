﻿using BankSystem.Job.Contracts;

namespace BankSystem.Job.QueryHandlers.GetUnsendedOutboxes
{
    /// <summary>
    /// Модель данных для ответа на запрос по получению всех сообщений об ошибке со статусом Unsended.
    /// </summary>
    public record GetUnsendedOutboxesResponseDto : IQueryDto
    {
        /// <summary>
        /// Список со всеми сообщениями об ошибке.
        /// </summary>
        public IReadOnlyCollection<GetUnsendedOutboxesSingleOutbox> Outboxes { get; init; }
    }
}
