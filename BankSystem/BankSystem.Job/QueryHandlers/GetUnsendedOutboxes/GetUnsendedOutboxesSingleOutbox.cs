﻿using BankSystem.Database.Helpers.Enums;

namespace BankSystem.Job.QueryHandlers.GetUnsendedOutboxes
{
    /// <summary>
    /// Модель данных для одного сообщения об изменениях.
    /// </summary>
    public record GetUnsendedOutboxesSingleOutbox
    {
        /// <summary>
        /// Идентификатор сообщения об изменениях.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Идентификатор события сообщения об изменениях.
        /// </summary>
        public Guid EventId { get; init; }

        /// <summary>
        /// Дата создания сообщения об изменениях.
        /// </summary>
        public DateTime OccuredOn { get; init; }

        /// <summary>
        /// JSON сообщение сообщения об изменениях.
        /// </summary>
        public string Message { get; init; }

        /// <summary>
        /// Тип сообщения об изменениях.
        /// </summary>
        public string MessageType { get; init; }

        /// <summary>
        /// Имя сборки сообщения об изменениях.
        /// </summary>
        public string AssemblyName { get; init; }

        /// <summary>
        /// Статус сообщения об изменениях.
        /// </summary>
        public OutboxStatus OutboxStatus { get; init; }
    }
}
