﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Helpers.Events.Contracts;
using BankSystem.Job.CommandHandlers.UpdateOutbox;
using BankSystem.Job.Contracts;
using BankSystem.Job.Settings;
using Confluent.Kafka;
using Confluent.Kafka.Admin;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Text;
using static Confluent.Kafka.ConfigPropertyNames;

namespace BankSystem.Job.Services
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class KafkaProducerService : IKafkaProducerService
    {
        private readonly IProducer<string, string> _producer;
        private readonly ICommandHandler<UpdateOutboxRequestDto> _updateOutboxCommandHandler;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="KafkaProducerService"/>
        /// </summary>
        /// <param name="updateOutboxCommandHandler"></param>
        public KafkaProducerService(ICommandHandler<UpdateOutboxRequestDto> updateOutboxCommandHandler,
                                    IProducerFactory producerFactory,
                                    IOptions<KafkaProducerSettings> options)
        {
            _updateOutboxCommandHandler = updateOutboxCommandHandler;

            _producer = producerFactory.Create<string, string>(options.Value.ProducerName);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="integrationEvent"><inheritdoc/></param>
        /// <param name="outboxId"><inheritdoc/></param>
        /// <returns></returns>
        public async Task SendMessage(IntegrationEvent integrationEvent, Guid outboxId)
        {
            try
            {
                string key = integrationEvent.EventId.ToString();

                if (integrationEvent.GetType().IsAssignableTo(typeof(IHasEntityId)))
                {
                    key = ((IHasEntityId)integrationEvent).EntityId.ToString();
                }

                var headers = new Headers()
                {
                    new Header(nameof(integrationEvent.EventId), Encoding.UTF8.GetBytes(integrationEvent.EventId.ToString())),
                    new Header(nameof(integrationEvent.OccuredOn), Encoding.UTF8.GetBytes(integrationEvent.OccuredOn.ToString("dd.MM.yyyy-HH:mm:ss")))
                };

                await _producer.ProduceAsync(integrationEvent.GetType().Name, new Message<string, string>
                {
                    Key = key,
                    Value = JsonConvert.SerializeObject(integrationEvent),
                    Headers = headers
                });

                await _updateOutboxCommandHandler.Handle(new UpdateOutboxRequestDto
                {
                    OutboxId = outboxId,
                    OutboxStatus = OutboxStatus.Sended
                });
            }
            catch
            {
                await _updateOutboxCommandHandler.Handle(new UpdateOutboxRequestDto
                {
                    OutboxId = outboxId,
                    OutboxStatus = OutboxStatus.SendingFailed
                });
            }
        }
    }
}
