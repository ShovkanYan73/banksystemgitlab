﻿using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Job.Contracts;
using BankSystem.Job.QueryHandlers;
using BankSystem.Job.QueryHandlers.GetUnsendedOutboxes;
using Newtonsoft.Json;
using System.Reflection;
using System.Threading;

namespace BankSystem.Job.Services
{
    public class OutboxHostedService : IHostedService
    {
        private readonly IQueryHandler<EmptyQueryRequestDto, GetUnsendedOutboxesResponseDto> _getUnsendedOutboxesQueryHandler;
        private readonly IKafkaProducerService _kafkaProducerService;
        private Timer? _timer = null;
        public OutboxHostedService(IQueryHandler<EmptyQueryRequestDto, GetUnsendedOutboxesResponseDto> getUnsendedOutboxesQueryHandler,
                                   IKafkaProducerService kafkaProducerService)
        {
            _getUnsendedOutboxesQueryHandler = getUnsendedOutboxesQueryHandler;
            _kafkaProducerService = kafkaProducerService;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(PublishIntegrationEvents, null, TimeSpan.Zero,
            TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        private async void PublishIntegrationEvents(object? state)
        {
            var outboxes = await _getUnsendedOutboxesQueryHandler.Handle(new EmptyQueryRequestDto());

            if (outboxes == null || outboxes.Outboxes == null || !outboxes.Outboxes.Any())
            {
                return;
            }

            foreach (var outbox in outboxes.Outboxes)
            {
                var messageType = Assembly.Load(outbox.AssemblyName).GetType(outbox.MessageType);
                
                if(messageType == null)
                {
                    throw new Exception("Тип не найден!");
                }

                if (!messageType.IsAssignableTo(typeof(IntegrationEvent)))
                {
                    throw new Exception("Этот тип сообщения не является интеграционным!");
                }

                var message = (IntegrationEvent)JsonConvert.DeserializeObject(outbox.Message, messageType)!;
                await _kafkaProducerService.SendMessage(message, outbox.Id);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }
    }
}
