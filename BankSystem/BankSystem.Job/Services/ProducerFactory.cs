﻿using BankSystem.Job.Contracts;
using BankSystem.Job.Settings;
using Confluent.Kafka;
using Microsoft.Extensions.Options;

namespace BankSystem.Job.Services
{
    public class ProducerFactory : IProducerFactory
    {
        private readonly ProducerListSettings _options;

        public ProducerFactory(IOptions<ProducerListSettings> options)
        {
            _options = options.Value;
        }

        public IProducer<TKey, TValue> Create<TKey, TValue>(string name)
        {
            return new ProducerBuilder<TKey, TValue>(new ProducerConfig
            {
                BootstrapServers = _options.Producers.First(x => x.Name.Equals(name)).BootstrapServers
            }).Build();
        }
    }
}
