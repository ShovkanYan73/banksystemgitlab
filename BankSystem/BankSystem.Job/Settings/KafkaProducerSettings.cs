﻿namespace BankSystem.Job.Settings
{
    public record KafkaProducerSettings
    {
        public string ProducerName { get; init; }
    }
}
