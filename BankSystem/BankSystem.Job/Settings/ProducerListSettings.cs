﻿namespace BankSystem.Job.Settings
{
    public record ProducerListSettings
    {
        public IReadOnlyCollection<ProducerSettings> Producers { get; init; }
    }
}
