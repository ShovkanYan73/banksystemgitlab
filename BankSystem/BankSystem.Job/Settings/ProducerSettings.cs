﻿namespace BankSystem.Job.Settings
{
    public record ProducerSettings
    {
        public string Name { get; init; }

        public string BootstrapServers { get; set; }
    }
}
