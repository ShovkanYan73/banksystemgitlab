﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Repositories;
using BankSystem.Database.Services.Contracts;
using BankSystem.Database.Services;
using BankSystem.Database;
using BankSystem.Job.CommandHandlers.UpdateOutbox;
using BankSystem.Job.Contracts;
using BankSystem.Job.Services;
using Microsoft.EntityFrameworkCore;
using BankSystem.Job.Automapper.Profiles;
using MediatR;
using System.Runtime;
using BankSystem.Job.Settings;

namespace BankSystem.Job
{
    public partial class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutofac();

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddAutoMapper(typeof(OutboxProfile).Assembly);

            services.AddDbContext<BankSystemDatabase>(x => x.UseNpgsql(Configuration.GetConnectionString("PostgreSQLConnection")));
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped(typeof(IReadRepository<>), typeof(ReadRepository<>));
            services.AddScoped(typeof(ICommandRepository<>), typeof(CommandRepository<>));
            services.AddScoped(typeof(IQueryRepository<>), typeof(QueryRepository<>));

            services.AddSingleton<IKafkaProducerService, KafkaProducerService>();
            services.AddHostedService<OutboxHostedService>();
            services.AddScoped<IProducerFactory, ProducerFactory>();

            services.Configure<ProducerListSettings>(Configuration.GetSection(nameof(ProducerListSettings)));
            services.Configure<KafkaProducerSettings>(Configuration.GetSection(nameof(KafkaProducerSettings)));
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(UpdateOutboxCommandHandler).Assembly)
                .Where(x => x.GetInterfaces().Any(_ => _.IsGenericType && (typeof(ICommandHandler<>) == _.GetGenericTypeDefinition() ||
                typeof(IQueryHandler<,>) == _.GetGenericTypeDefinition())))
                .AsImplementedInterfaces();
        }

    }
}
