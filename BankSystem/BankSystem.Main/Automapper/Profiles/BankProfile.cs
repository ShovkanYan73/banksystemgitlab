﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Main.Commands.UpdateBank;
using BankSystem.Main.Queries.GetAllBanks;
using BankSystem.Main.Queries.GetBankById;

namespace BankSystem.Main.Automapper.Profiles
{
    public class BankProfile : Profile
    {
        public BankProfile()
        {
            CreateMap<Bank, GetAllBanksSingleBankDto>();
            CreateMap<Bank, GetBankByIdResponseDto>();
            CreateMap<UpdateBankBankDto, UpdateBankDto>();
        }
    }
}
