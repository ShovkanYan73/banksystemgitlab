﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Main.Commands.UpdateBank;
using BankSystem.Main.Commands.UpdateDiscount;
using BankSystem.Main.Queries.GetAllDiscounts;
using BankSystem.Main.Queries.GetBankById;

namespace BankSystem.Main.Automapper.Profiles
{
    public class DiscountProfile :Profile
    {
        public DiscountProfile()
        {
            CreateMap<Discount, GetAllDiscountsSingleDiscount>();
            CreateMap<UpdateDiscountDiscountDto, UpdateDiscountDto>();
        }
    }
}
