﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Main.Commands.UpdateBank;
using BankSystem.Main.Commands.UpdateProduct;
using BankSystem.Main.Queries.GetAllProducts;
using BankSystem.Main.Queries.GetBankById;

namespace BankSystem.Main.Automapper.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, GetAllProductsSingleProduct>();
            CreateMap<UpdateProductProductDto, UpdateProductDto>();
        }
    }
}
