﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Main.Commands.UpdateBank;
using BankSystem.Main.Commands.UpdateSubproduct;
using BankSystem.Main.Queries.GetAllSubproducts;
using BankSystem.Main.Queries.GetBankById;

namespace BankSystem.Main.Automapper.Profiles
{
    public class SubproductProfile : Profile
    {
        public SubproductProfile()
        {
            CreateMap<Subproduct, GetAllSubproductsSingleSubproduct>();
            CreateMap<UpdateSubproductSubproductDto, UpdateSubproductDto>();
        }
    }
}
