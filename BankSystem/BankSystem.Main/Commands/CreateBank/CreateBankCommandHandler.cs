﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateBank
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class CreateBankCommandHandler : ICommandHandler<CreateBankRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;
        
        /// <summary>
        /// Создание экземпляра объекта типа <see cref="CreateBankCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public CreateBankCommandHandler(ICommandRepository<Bank> repository,
                                        IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        public async Task Handle(CreateBankRequestDto model)
        {
            await _repository.CreateAsync(new Bank(model.Name));
            await _unitOfWork.CommitAsync();
        }
    }
}
