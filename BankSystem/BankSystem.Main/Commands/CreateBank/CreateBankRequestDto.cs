﻿using BankSystem.Database.Models;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateBank
{
    /// <summary>
    /// Модель данных для команды на создание банка.
    /// </summary>
    public record CreateBankRequestDto : ICommandDto
    {
        /// <summary>
        /// Наименование банка.
        /// </summary>
        public string Name { get; init; } = string.Empty;
    }
}
