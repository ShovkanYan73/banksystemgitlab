﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateDiscount
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class CreateDiscountCommandHandler : ICommandHandler<CreateDiscountRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;
        
        /// <summary>
        /// Создание экземпляра объекта типа <see cref="CreateDiscountCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public CreateDiscountCommandHandler(ICommandRepository<Bank> repository,
                                            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(CreateDiscountRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }

            bank.AddDiscount(new Discount(model.Discount.Name ,model.Discount.DiscountRate));
            await _unitOfWork.CommitAsync();
        }
    }
}
