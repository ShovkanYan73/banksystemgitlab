﻿namespace BankSystem.Main.Commands.CreateDiscount
{
    /// <summary>
    /// Модель данных с информацией о скидке.
    /// </summary>
    public record CreateDiscountDiscountDto
    {
        /// <summary>
        /// Название скидки.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Размер скидки в процентах.
        /// </summary>
        public decimal DiscountRate { get; init; }
    }
}
