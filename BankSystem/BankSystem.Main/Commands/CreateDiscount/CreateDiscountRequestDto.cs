﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateDiscount
{
    /// <summary>
    /// Модель данных для команды на создание скидки.
    /// </summary>
    public record CreateDiscountRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Информация о скидке.
        /// </summary>
        public CreateDiscountDiscountDto Discount { get; init; }
    }
}
