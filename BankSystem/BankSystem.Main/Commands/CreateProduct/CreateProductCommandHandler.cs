﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Commands.CreateBank;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateProduct
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class CreateProductCommandHandler : ICommandHandler<CreateProductRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;
        
        /// <summary>
        /// Создание экземпляра объекта типа <see cref="CreateProductCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public CreateProductCommandHandler(ICommandRepository<Bank> repository,
                                           IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        public async Task Handle(CreateProductRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }

            bank.AddProduct(new Product(model.Product.Name));
            await _unitOfWork.CommitAsync();
        }
    }
}
