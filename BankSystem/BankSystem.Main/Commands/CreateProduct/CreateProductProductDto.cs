﻿namespace BankSystem.Main.Commands.CreateProduct
{
    /// <summary>
    /// Модель данных с информацией о продукте.
    /// </summary>
    public record CreateProductProductDto
    {
        /// <summary>
        /// Наименование продукта.
        /// </summary>
        public string Name { get; init; }
    }
}
