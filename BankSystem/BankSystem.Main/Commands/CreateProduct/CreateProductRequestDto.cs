﻿using BankSystem.Database.Models;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateProduct
{
    /// <summary>
    /// Модель данных для команды на создание продукта.
    /// </summary>
    public record CreateProductRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Информация о продукте.
        /// </summary>
        public CreateProductProductDto Product { get; init; }
    }
}
