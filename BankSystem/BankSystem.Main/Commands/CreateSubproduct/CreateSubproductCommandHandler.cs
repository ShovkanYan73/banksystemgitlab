﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateSubproduct
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class CreateSubproductCommandHandler : ICommandHandler<CreateSubproductRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="CreateSubproductCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public CreateSubproductCommandHandler(ICommandRepository<Bank> repository,
                                              IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(CreateSubproductRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }

            bank.AddSubproduct(model.ProductId, new Subproduct(model.Subproduct.Name, model.Subproduct.Rate));
            await _unitOfWork.CommitAsync();
        }
    }
}
