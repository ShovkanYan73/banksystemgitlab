﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.CreateSubproduct
{
    /// <summary>
    /// Модель данных для команды на создание субпродукта.
    /// </summary>
    public record CreateSubproductRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid ProductId { get; init; }
        
        /// <summary>
        /// Информация о субпродукте.
        /// </summary>
        public CreateSubproductSubproductDto Subproduct { get; init; }
    }
}
