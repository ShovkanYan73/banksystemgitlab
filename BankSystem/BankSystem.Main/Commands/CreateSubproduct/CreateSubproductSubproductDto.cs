﻿namespace BankSystem.Main.Commands.CreateSubproduct
{
    /// <summary>
    /// Модель данных с информацией о субпродукте.
    /// </summary>
    public record CreateSubproductSubproductDto
    {
        /// <summary>
        /// Наименование субпродукта.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Процентная ставка субпродукта.
        /// </summary>
        public decimal Rate { get; init; }
    }
}
