﻿using AutoMapper;
using BankSystem.Database.Helpers.Events;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Commands.CreateBank;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteBank
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class DeleteBankCommandHandler : ICommandHandler<DeleteBankRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;
        /// <summary>
        /// Создание экземпляра объекта типа <see cref="DeleteBankCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public DeleteBankCommandHandler(ICommandRepository<Bank> repository,
                                        IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(DeleteBankRequestDto model)
        {
            var bankToDelete = await _repository.GetByIdAsync(model.BankId);

            if(bankToDelete == null)
            {
                throw new Exception("Удаляемого банка не существует в БД.");
            }

            bankToDelete.DeleteBank();
            await _unitOfWork.CommitAsync();
        }
    }
}
