﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteBank
{
    /// <summary>
    /// Модель данных для команды на удаление банка.
    /// </summary>
    public record DeleteBankRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }
    }
}
