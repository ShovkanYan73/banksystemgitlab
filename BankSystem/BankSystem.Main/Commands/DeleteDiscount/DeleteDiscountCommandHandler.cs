﻿using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteDiscount
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class DeleteDiscountCommandHandler : ICommandHandler<DeleteDiscountRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="DeleteDiscountCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public DeleteDiscountCommandHandler(ICommandRepository<Bank> repository,
                                            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(DeleteDiscountRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }

            bank.DeleteDiscount(model.DiscountId);
            _repository.Update(bank);
            await _unitOfWork.CommitAsync();
        }
    }
}
