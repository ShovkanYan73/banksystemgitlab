﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteDiscount
{
    /// <summary>
    /// Модель данных для команды на удаление скидки.
    /// </summary>
    public class DeleteDiscountRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор скидки.
        /// </summary>
        public Guid DiscountId { get; init; }
    }
}
