﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteProduct
{
    /// <summary>
    /// Модель данных для команды на удаление продукта.
    /// </summary>
    public record DeleteProductRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid ProductId { get; init; }
    }
}
