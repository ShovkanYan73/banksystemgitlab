﻿using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteSubproduct
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class DeleteSubproductCommandHandler : ICommandHandler<DeleteSubproductRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="DeleteSubproductCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        public DeleteSubproductCommandHandler(ICommandRepository<Bank> repository,
                                              IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(DeleteSubproductRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }

            bank.DeleteSubproduct(model.ProductId, model.SubproductId);
            await _unitOfWork.CommitAsync();
        }
    }
}
