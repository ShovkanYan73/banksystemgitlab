﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.DeleteSubproduct
{
    /// <summary>
    /// Модель данных для команды на удаление субпродукта.
    /// </summary>
    public record DeleteSubproductRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid ProductId { get; init; }

        /// <summary>
        /// Идентификатор субпродукта.
        /// </summary>
        public Guid SubproductId { get; init; }
    }
}
