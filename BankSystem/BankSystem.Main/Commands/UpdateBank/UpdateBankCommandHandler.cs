﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Commands.CreateBank;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateBank
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class UpdateBankCommandHandler : ICommandHandler<UpdateBankRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="UpdateBankCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public UpdateBankCommandHandler(ICommandRepository<Bank> repository,
                                        IMapper mapper,
                                        IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        public async Task Handle(UpdateBankRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.Id);

            if (bank == null)
            {
                throw new Exception("Обновляемого банка не существует в БД.");
            }

            bank.UpdateBank(_mapper.Map<UpdateBankDto>(model.Bank));
            _repository.Update(bank);
            await _unitOfWork.CommitAsync();
        }
    }
}
