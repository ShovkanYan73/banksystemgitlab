﻿using BankSystem.Database.Models;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateBank
{
    /// <summary>
    /// Модель данных для команды на обновление банка.
    /// </summary>
    public record UpdateBankRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Модель банка для обновления.
        /// </summary>
        public UpdateBankBankDto Bank { get; init; }
    }
}
