﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateDiscount
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class UpdateDiscountCommandHandler : ICommandHandler<UpdateDiscountRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="UpdateDiscountCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public UpdateDiscountCommandHandler(ICommandRepository<Bank> repository,
                                        IMapper mapper,
                                        IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(UpdateDiscountRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }
            var updatedDiscount = _mapper.Map<UpdateDiscountDto>(model.Discount);
            updatedDiscount.DiscountId = model.DiscountId;

            bank.UpdateDiscount(updatedDiscount);
            _repository.Update(bank);
            await _unitOfWork.CommitAsync();
        }
    }
}
