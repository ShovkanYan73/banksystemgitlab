﻿namespace BankSystem.Main.Commands.UpdateDiscount
{
    /// <summary>
    /// Модель данных с информацией о скидке.
    /// </summary>
    public record UpdateDiscountDiscountDto
    {
        /// <summary>
        /// Название скидки.
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// Размер скидки в процентах.
        /// </summary>
        public decimal? DiscountRate { get; init; }
    }
}
