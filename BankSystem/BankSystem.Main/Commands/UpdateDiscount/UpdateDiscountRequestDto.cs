﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateDiscount
{
    /// <summary>
    /// Модель данных для команды на обновление скидки.
    /// </summary>
    public class UpdateDiscountRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор скидки.
        /// </summary>
        public Guid DiscountId { get; init; }

        /// <summary>
        /// Модель скидки.
        /// </summary>
        public UpdateDiscountDiscountDto Discount { get; init; }
    }
}
