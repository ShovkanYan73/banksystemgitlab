﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateProduct
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class UpdateProductCommandHandler : ICommandHandler<UpdateProductRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="UpdateProductCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public UpdateProductCommandHandler(ICommandRepository<Bank> repository,
                                           IMapper mapper,
                                           IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task Handle(UpdateProductRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }
            var updatedProduct = _mapper.Map<UpdateProductDto>(model.Product);
            updatedProduct.ProductId = model.ProductId;
            bank.UpdateProduct(updatedProduct);
            _repository.Update(bank);
            await _unitOfWork.CommitAsync();
        }
    }
}
