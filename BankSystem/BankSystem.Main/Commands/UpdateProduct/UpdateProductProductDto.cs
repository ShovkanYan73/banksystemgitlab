﻿using BankSystem.Database.Models;

namespace BankSystem.Main.Commands.UpdateProduct
{
    /// <summary>
    /// Модель данных с информацией о продукте.
    /// </summary>
    public record UpdateProductProductDto
    {
        /// <summary>
        /// Наименование продукта.
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// Список субпродуктов продукта.
        /// </summary>
        public List<Subproduct>? Subproducts { get; init; }
    }
}
