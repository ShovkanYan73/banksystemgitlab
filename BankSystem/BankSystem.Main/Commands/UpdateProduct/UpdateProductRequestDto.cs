﻿using BankSystem.Database.Models;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateProduct
{
    /// <summary>
    /// Модель данных для команды на обновление продукта.
    /// </summary>
    public record UpdateProductRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid ProductId { get; init; }

        /// <summary>
        /// Информация о продукте.
        /// </summary>
        public UpdateProductProductDto Product { get; init; }
    }
}
