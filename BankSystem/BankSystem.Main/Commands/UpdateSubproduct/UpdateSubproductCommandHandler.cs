﻿using AutoMapper;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Services.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateSubproduct
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class UpdateSubproductCommandHandler : ICommandHandler<UpdateSubproductRequestDto>
    {
        private readonly ICommandRepository<Bank> _repository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="UpdateSubproductCommandHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public UpdateSubproductCommandHandler(ICommandRepository<Bank> repository,
                                              IMapper mapper,
                                              IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(UpdateSubproductRequestDto model)
        {
            var bank = await _repository.GetByIdAsync(model.BankId);

            if (bank == null)
            {
                throw new Exception("Банка не существует в БД.");
            }

            var updatedSubproduct = _mapper.Map<UpdateSubproductDto>(model.Subproduct);
            updatedSubproduct.ProductId = model.ProductId;
            updatedSubproduct.SubproductId = model.SubproductId;

            bank.UpdateSubproduct(updatedSubproduct);
            _repository.Update(bank);
            await _unitOfWork.CommitAsync();
        }
    }
}
