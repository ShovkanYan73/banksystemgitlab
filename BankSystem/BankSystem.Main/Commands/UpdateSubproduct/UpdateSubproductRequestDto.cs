﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Commands.UpdateSubproduct
{
    /// <summary>
    /// Модель данных для команды на обновление субпродукта.
    /// </summary>
    public class UpdateSubproductRequestDto : ICommandDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }

        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid ProductId { get; init; }

        /// <summary>
        /// Идентификатор субпродукта.
        /// </summary>
        public Guid SubproductId { get; init; }

        /// <summary>
        /// Информация о субпродукте.
        /// </summary>
        public UpdateSubproductSubproductDto Subproduct { get; init; }
    }
}
