﻿namespace BankSystem.Main.Commands.UpdateSubproduct
{
    /// <summary>
    /// Модель данных с информацией о субпродукте.
    /// </summary>
    public record UpdateSubproductSubproductDto
    {
        /// <summary>
        /// Наименование субпродукта.
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// Процентная ставка субпродукта.
        /// </summary>
        public decimal? Rate { get; init; }
    }
}
