﻿using BankSystem.Main.Commands.CreateBank;
using BankSystem.Main.Commands.DeleteBank;
using BankSystem.Main.Commands.UpdateBank;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries;
using BankSystem.Main.Queries.GetAllBanks;
using BankSystem.Main.Queries.GetBankById;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankSystem.Main.Controllers
{
    [ApiController]
    [Route("api/banks")]
    public class BankController : Controller
    {
        private readonly ICommandHandler<CreateBankRequestDto> _createBankCommandHandler;
        private readonly ICommandHandler<UpdateBankRequestDto> _updateBankCommandHandler;
        private readonly ICommandHandler<DeleteBankRequestDto> _deleteBankCommandHandler;
        private readonly IQueryHandler<EmptyQueryRequestDto, GetAllBanksResponseDto> _getAllBanksQueryHandler;
        private readonly IQueryHandler<GetBankByIdRequestDto, GetBankByIdResponseDto> _getBankByIdQueryHandler;


        public BankController(ICommandHandler<CreateBankRequestDto> createBankCommandHandler,
                              ICommandHandler<UpdateBankRequestDto> updateBankCommandHandler,
                              ICommandHandler<DeleteBankRequestDto> deleteBankCommandHandler,
                              IQueryHandler<EmptyQueryRequestDto, GetAllBanksResponseDto> getAllBanksQueryHandler,
                              IQueryHandler<GetBankByIdRequestDto, GetBankByIdResponseDto> getBankByIdQueryHandler)
        {
            _createBankCommandHandler = createBankCommandHandler;
            _updateBankCommandHandler = updateBankCommandHandler;
            _deleteBankCommandHandler = deleteBankCommandHandler;
            _getAllBanksQueryHandler = getAllBanksQueryHandler;
            _getBankByIdQueryHandler = getBankByIdQueryHandler;
        }

        [HttpPost]
        public async Task Create(CreateBankRequestDto model)
            => await _createBankCommandHandler.Handle(model);

        [HttpPut("{id}")]
        public async Task Update(Guid id, UpdateBankBankDto model)
            => await _updateBankCommandHandler.Handle(new UpdateBankRequestDto
            {
                Id = id,
                Bank = model
            });

        [HttpDelete("id")]
        public async Task Delete(Guid id)
            => await _deleteBankCommandHandler.Handle(new DeleteBankRequestDto
            {
                BankId = id
            });

        [HttpGet]
        public async Task<GetAllBanksResponseDto> GetAll()
            => await _getAllBanksQueryHandler.Handle(new EmptyQueryRequestDto());

        [HttpGet("{id}")]
        public async Task<GetBankByIdResponseDto> GetById(Guid id)
            => await _getBankByIdQueryHandler.Handle(new GetBankByIdRequestDto
            {
                BankId = id
            });

    }
}
