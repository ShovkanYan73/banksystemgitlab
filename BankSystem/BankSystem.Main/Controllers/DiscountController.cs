﻿using BankSystem.Main.Commands.CreateBank;
using BankSystem.Main.Commands.DeleteBank;
using BankSystem.Main.Commands.UpdateBank;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries.GetAllBanks;
using BankSystem.Main.Queries.GetBankById;
using BankSystem.Main.Queries;
using Microsoft.AspNetCore.Mvc;
using BankSystem.Main.Commands.CreateDiscount;
using BankSystem.Main.Commands.UpdateDiscount;
using BankSystem.Main.Commands.DeleteDiscount;
using BankSystem.Main.Queries.GetAllDiscounts;

namespace BankSystem.Main.Controllers
{
    [ApiController]
    [Route("api/banks/{bankId}/discounts")]
    public class DiscountController : Controller
    {
        private readonly ICommandHandler<CreateDiscountRequestDto> _createDiscountCommandHandler;
        private readonly ICommandHandler<UpdateDiscountRequestDto> _updateDiscountCommandHandler;
        private readonly ICommandHandler<DeleteDiscountRequestDto> _deleteDiscountCommandHandler;
        private readonly IQueryHandler<EmptyQueryRequestDto, GetAllDiscountsResponseDto> _getAllDiscountsQueryHandler;


        public DiscountController(ICommandHandler<CreateDiscountRequestDto> createDiscountCommandHandler,
                              ICommandHandler<UpdateDiscountRequestDto> updateDiscountCommandHandler,
                              ICommandHandler<DeleteDiscountRequestDto> deleteDiscountCommandHandler,
                              IQueryHandler<EmptyQueryRequestDto, GetAllDiscountsResponseDto> getAllDiscountsQueryHandler)
        {
            _createDiscountCommandHandler = createDiscountCommandHandler;
            _updateDiscountCommandHandler = updateDiscountCommandHandler;
            _deleteDiscountCommandHandler = deleteDiscountCommandHandler;
            _getAllDiscountsQueryHandler = getAllDiscountsQueryHandler;
        }

        [HttpPost]
        public async Task Create(Guid bankId, CreateDiscountDiscountDto model)
            => await _createDiscountCommandHandler.Handle(new CreateDiscountRequestDto
            {
                BankId = bankId,
                Discount = model
            });

        [HttpPut("{id}")]
        public async Task Update(Guid bankId, Guid id, UpdateDiscountDiscountDto model)
            => await _updateDiscountCommandHandler.Handle(new UpdateDiscountRequestDto
            {
                BankId = bankId,
                DiscountId = id,
                Discount = model
            });

        [HttpDelete("{id}")]
        public async Task Delete(Guid bankId, Guid id) 
            => await _deleteDiscountCommandHandler.Handle(new DeleteDiscountRequestDto
            {
                BankId = bankId,
                DiscountId = id
            });

        [HttpGet]
        public async Task<GetAllDiscountsResponseDto> GetAll()
            => await _getAllDiscountsQueryHandler.Handle(new EmptyQueryRequestDto());
    }
}
