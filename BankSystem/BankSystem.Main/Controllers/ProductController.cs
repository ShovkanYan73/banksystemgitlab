﻿using BankSystem.Main.Commands.CreateDiscount;
using BankSystem.Main.Commands.DeleteDiscount;
using BankSystem.Main.Commands.UpdateDiscount;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries.GetAllDiscounts;
using BankSystem.Main.Queries;
using Microsoft.AspNetCore.Mvc;
using BankSystem.Main.Commands.CreateProduct;
using BankSystem.Main.Commands.UpdateProduct;
using BankSystem.Main.Commands.DeleteProduct;
using BankSystem.Main.Queries.GetAllProducts;

namespace BankSystem.Main.Controllers
{
    [ApiController]
    [Route("api/banks/{bankId}/products")]
    public class ProductController : Controller
    {
        private readonly ICommandHandler<CreateProductRequestDto> _createProductCommandHandler;
        private readonly ICommandHandler<UpdateProductRequestDto> _updateProductCommandHandler;
        private readonly ICommandHandler<DeleteProductRequestDto> _deleteProductCommandHandler;
        private readonly IQueryHandler<EmptyQueryRequestDto, GetAllProductsResponseDto> _getAllProductsQueryHandler;


        public ProductController(ICommandHandler<CreateProductRequestDto> createProductCommandHandler,
                              ICommandHandler<UpdateProductRequestDto> updateProductCommandHandler,
                              ICommandHandler<DeleteProductRequestDto> deleteProductCommandHandler,
                              IQueryHandler<EmptyQueryRequestDto, GetAllProductsResponseDto> getAllProductsQueryHandler)
        {
            _createProductCommandHandler = createProductCommandHandler;
            _updateProductCommandHandler = updateProductCommandHandler;
            _deleteProductCommandHandler = deleteProductCommandHandler;
            _getAllProductsQueryHandler = getAllProductsQueryHandler;
        }

        [HttpPost]
        public async Task CreateProduct(Guid bankId, CreateProductProductDto model)
            => await _createProductCommandHandler.Handle(new CreateProductRequestDto
            {
                BankId = bankId,
                Product = model
            });

        [HttpPut("{id}")]
        public async Task UpdateProduct(Guid bankId, Guid id, UpdateProductProductDto model)
            => await _updateProductCommandHandler.Handle(new UpdateProductRequestDto
            {
                BankId = bankId,
                ProductId = id,
                Product = model
            });

        [HttpDelete("{id}")]
        public async Task DeleteProduct(Guid bankId, Guid id)
            => await _deleteProductCommandHandler.Handle(new DeleteProductRequestDto
            {
                BankId = bankId,
                ProductId = id
            });

        [HttpGet]
        public async Task<GetAllProductsResponseDto> GetAllProducts()
            => await _getAllProductsQueryHandler.Handle(new EmptyQueryRequestDto());
    }
}
