﻿using BankSystem.Main.Commands.CreateProduct;
using BankSystem.Main.Commands.DeleteProduct;
using BankSystem.Main.Commands.UpdateProduct;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries.GetAllProducts;
using BankSystem.Main.Queries;
using Microsoft.AspNetCore.Mvc;
using BankSystem.Main.Commands.CreateSubproduct;
using BankSystem.Main.Commands.UpdateSubproduct;
using BankSystem.Main.Commands.DeleteSubproduct;
using BankSystem.Main.Queries.GetAllSubproducts;

namespace BankSystem.Main.Controllers
{
    [ApiController]
    [Route("api/banks/{bankId}/products/{productId}/subproducts")]
    public class SubproductController : Controller
    {
        private readonly ICommandHandler<CreateSubproductRequestDto> _createSubproductCommandHandler;
        private readonly ICommandHandler<UpdateSubproductRequestDto> _updateSubproductCommandHandler;
        private readonly ICommandHandler<DeleteSubproductRequestDto> _deleteSubproductCommandHandler;
        private readonly IQueryHandler<EmptyQueryRequestDto, GetAllSubproductsResponseDto> _getAllSubproductsQueryHandler;


        public SubproductController(ICommandHandler<CreateSubproductRequestDto> createSubproductCommandHandler,
                              ICommandHandler<UpdateSubproductRequestDto> updateSubproductCommandHandler,
                              ICommandHandler<DeleteSubproductRequestDto> deleteSubproductCommandHandler,
                              IQueryHandler<EmptyQueryRequestDto, GetAllSubproductsResponseDto> getAllSubproductsQueryHandler)
        {
            _createSubproductCommandHandler = createSubproductCommandHandler;
            _updateSubproductCommandHandler = updateSubproductCommandHandler;
            _deleteSubproductCommandHandler = deleteSubproductCommandHandler;
            _getAllSubproductsQueryHandler = getAllSubproductsQueryHandler;
        }

        [HttpPost]
        public async Task CreateSubproduct(Guid bankId, Guid productId, CreateSubproductSubproductDto model)
            => await _createSubproductCommandHandler.Handle(new CreateSubproductRequestDto
            {
                BankId = bankId,
                ProductId = productId,
                Subproduct = model
            });

        [HttpPut("{id}")]
        public async Task UpdateSubproduct(Guid bankId, Guid productId, Guid id, UpdateSubproductSubproductDto model)
            => await _updateSubproductCommandHandler.Handle(new UpdateSubproductRequestDto
            {
                BankId = bankId,
                ProductId = productId,
                SubproductId = id,
                Subproduct = model
            });

        [HttpDelete("{id}")]
        public async Task DeleteSubproduct(Guid bankId, Guid productId, Guid id)
            => await _deleteSubproductCommandHandler.Handle(new DeleteSubproductRequestDto
            {
                BankId = bankId,
                ProductId = productId,
                SubproductId = id
            });

        [HttpGet]
        public async Task<GetAllSubproductsResponseDto> GetAllSubproducts() 
            => await _getAllSubproductsQueryHandler.Handle(new EmptyQueryRequestDto());
    }
}
