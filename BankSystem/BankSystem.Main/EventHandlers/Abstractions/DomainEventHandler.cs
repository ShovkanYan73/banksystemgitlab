﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using MediatR;
using Newtonsoft.Json;

namespace BankSystem.Main.EventHandlers.Abstractions
{
    public abstract class DomainEventHandler<TRequest> : IRequestHandler<TRequest> where TRequest : DomainEvent
    {
        private readonly ICommandRepository<Log> _repository;

        public DomainEventHandler(ICommandRepository<Log> repository)
        {
            _repository = repository;
        }

        public abstract Log BuildLog(TRequest request);

        public virtual async Task<Unit> Handle(TRequest request, CancellationToken cancellationToken = default)
        {
            var log = BuildLog(request);
            await _repository.CreateAsync(log, cancellationToken);
            return Unit.Value;
        }
    }
}
