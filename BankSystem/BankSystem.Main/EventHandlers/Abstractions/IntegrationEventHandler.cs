﻿using BankSystem.Database.Helpers.Events.Abstractions;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using MediatR;
using Newtonsoft.Json;

namespace BankSystem.Main.EventHandlers.Abstractions
{
    public class IntegrationEventHandler<TRequest> : IRequestHandler<TRequest> where TRequest : IntegrationEvent
    {
        private readonly ICommandRepository<Outbox> _repository;

        public IntegrationEventHandler(ICommandRepository<Outbox> repository)
        {
            _repository = repository;
        }

        public virtual async Task<Unit> Handle(TRequest request, CancellationToken cancellationToken = default)
        {
            var outbox = new Outbox(request.EventId, request.OccuredOn, JsonConvert.SerializeObject(request), request.GetType().FullName!, request.GetType().Assembly.FullName!);
            await _repository.CreateAsync(outbox, cancellationToken);
            return Unit.Value;
        }
    }
}
