﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using Newtonsoft.Json;
using BankSystem.Main.EventHandlers.Abstractions;
using BankSystem.Database.Helpers.Events.DomainEvents;

namespace BankSystem.Main.EventHandlers.DomainEventHandlers
{
    public class DeleteBankDomainEventHandler : DomainEventHandler<BankDeletedDomainEvent>
    {
        public DeleteBankDomainEventHandler(ICommandRepository<Log> repository) : base(repository) { }

        public override Log BuildLog(BankDeletedDomainEvent request)
        {
            return new Log(request.EventId, request.OccuredOn, JsonConvert.SerializeObject(request), request.GetType().FullName!, ActionType.Deleted);
        }
    }
}
