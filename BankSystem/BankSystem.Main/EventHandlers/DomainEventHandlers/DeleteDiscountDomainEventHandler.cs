﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;
using Newtonsoft.Json;

namespace BankSystem.Main.EventHandlers.DomainEventHandlers
{
    public class DeleteDiscountDomainEventHandler : DomainEventHandler<DiscountDeletedDomainEvent>
    {
        public DeleteDiscountDomainEventHandler(ICommandRepository<Log> repository) : base(repository) { }

        public override Log BuildLog(DiscountDeletedDomainEvent request)
        {
            return new Log(request.EventId, request.OccuredOn, JsonConvert.SerializeObject(request), request.GetType().FullName!, ActionType.Deleted);
        }
    }
}
