﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;
using Newtonsoft.Json;

namespace BankSystem.Main.EventHandlers.DomainEventHandlers
{
    public class DeleteProductDomainEventHandler : DomainEventHandler<ProductDeletedDomainEvent>
    {
        public DeleteProductDomainEventHandler(ICommandRepository<Log> repository) : base(repository) { }

        public override Log BuildLog(ProductDeletedDomainEvent request)
        {
            return new Log(request.EventId, request.OccuredOn, JsonConvert.SerializeObject(request), request.GetType().FullName!, ActionType.Deleted);
        }
    }
}
