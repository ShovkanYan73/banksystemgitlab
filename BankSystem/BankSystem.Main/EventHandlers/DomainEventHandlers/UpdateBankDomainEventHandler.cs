﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;
using Newtonsoft.Json;

namespace BankSystem.Main.EventHandlers.DomainEventHandlers
{
    public class UpdateBankDomainEventHandler : DomainEventHandler<BankUpdatedDomainEvent>
    {
        public UpdateBankDomainEventHandler(ICommandRepository<Log> repository) : base(repository) { }

        public override Log BuildLog(BankUpdatedDomainEvent request)
        {
            return new Log(request.EventId, request.OccuredOn, JsonConvert.SerializeObject(request), request.GetType().FullName!, ActionType.Updated);
        }
    }
}
