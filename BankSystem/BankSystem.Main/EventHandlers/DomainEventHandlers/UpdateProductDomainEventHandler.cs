﻿using BankSystem.Database.Helpers.Enums;
using BankSystem.Database.Helpers.Events.DomainEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;
using Newtonsoft.Json;

namespace BankSystem.Main.EventHandlers.DomainEventHandlers
{
    public class UpdateProductDomainEventHandler : DomainEventHandler<ProductUpdatedDomainEvent>
    {
        public UpdateProductDomainEventHandler(ICommandRepository<Log> repository) : base(repository) { }

        public override Log BuildLog(ProductUpdatedDomainEvent request)
        {
            return new Log(request.EventId, request.OccuredOn, JsonConvert.SerializeObject(request), request.GetType().FullName!, ActionType.Updated);
        }
    }
}
