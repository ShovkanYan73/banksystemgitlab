﻿using BankSystem.Database.Helpers.Events.IntegrationEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;

namespace BankSystem.Main.EventHandlers.IntegrationEventHandlers
{
    public class DeleteBankIntegrationEventHandler : IntegrationEventHandler<BankDeletedIntegrationEvent>
    {
        public DeleteBankIntegrationEventHandler(ICommandRepository<Outbox> repository) : base(repository) { }
    }
}
