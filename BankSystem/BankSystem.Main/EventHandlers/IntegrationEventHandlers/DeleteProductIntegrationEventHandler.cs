﻿using BankSystem.Database.Helpers.Events.IntegrationEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;

namespace BankSystem.Main.EventHandlers.IntegrationEventHandlers
{
    public class DeleteProductIntegrationEventHandler : IntegrationEventHandler<ProductDeletedIntegrationEvent>
    {
        public DeleteProductIntegrationEventHandler(ICommandRepository<Outbox> repository) : base(repository) { }
    }
}
