﻿using BankSystem.Database.Helpers.Events.IntegrationEvents;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.EventHandlers.Abstractions;

namespace BankSystem.Main.EventHandlers.IntegrationEventHandlers
{
    public class UpdateProductIntegrationEventHandler : IntegrationEventHandler<ProductUpdatedIntegrationEvent>
    {
        public UpdateProductIntegrationEventHandler(ICommandRepository<Outbox> repository) : base(repository) { }
    }
}
