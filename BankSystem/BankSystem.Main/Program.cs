using Autofac.Extensions.DependencyInjection;
using BankSystem.Database;
using Microsoft.EntityFrameworkCore;

namespace BankSystem.Main
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = CreateHostBuilder(args).Build();

            using var scope = builder.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<BankSystemDatabase>();
            context.Database.Migrate();

            builder.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).UseServiceProviderFactory(new AutofacServiceProviderFactory());
    }
}




