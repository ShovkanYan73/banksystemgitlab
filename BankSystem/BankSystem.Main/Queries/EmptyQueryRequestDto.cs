﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries
{
    /// <summary>
    /// Пустая модель данных для запроса.
    /// </summary>
    public class EmptyQueryRequestDto : IQueryDto
    {
    }
}
