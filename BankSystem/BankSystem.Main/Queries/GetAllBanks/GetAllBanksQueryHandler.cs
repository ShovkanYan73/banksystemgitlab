﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries.GetBankById;

namespace BankSystem.Main.Queries.GetAllBanks
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class GetAllBanksQueryHandler : IQueryHandler<EmptyQueryRequestDto, GetAllBanksResponseDto>
    {
        private readonly IQueryRepository<Bank> _repository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="GetAllBanksQueryHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public GetAllBanksQueryHandler(IQueryRepository<Bank> repository,
                                       IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        public async Task<GetAllBanksResponseDto> Handle(EmptyQueryRequestDto model)
        {
            return new GetAllBanksResponseDto
            {
                Banks = _mapper.Map<IReadOnlyCollection<GetAllBanksSingleBankDto>>(await _repository.GetAllAsync())
            };
        }
    }
}

