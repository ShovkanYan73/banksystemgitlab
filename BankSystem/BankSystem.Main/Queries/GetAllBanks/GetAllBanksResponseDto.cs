﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllBanks
{
    /// <summary>
    /// Модель данных для ответа на запрос по получению всех банков.
    /// </summary>
    public record GetAllBanksResponseDto : IQueryDto
    {
        /// <summary>
        /// Список со всеми банками.
        /// </summary>
        public IReadOnlyCollection<GetAllBanksSingleBankDto> Banks { get; init; }
    }
}
