﻿using BankSystem.Database.Models;

namespace BankSystem.Main.Queries.GetAllBanks
{
    /// <summary>
    /// Модель данных для одного банка.
    /// </summary>
    public record GetAllBanksSingleBankDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Наименование банка.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Список офисов банка.
        /// </summary>
        public List<Office> Offices { get; init; }

        /// <summary>
        /// Список продукции банка.
        /// </summary>
        public List<Product> Products { get; init; }

        /// <summary>
        /// Список скидок банка.
        /// </summary>
        public List<Discount> Discounts { get; init; }
    }
}
