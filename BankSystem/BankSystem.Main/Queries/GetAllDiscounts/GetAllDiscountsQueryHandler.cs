﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries.GetAllProducts;

namespace BankSystem.Main.Queries.GetAllDiscounts
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class GetAllDiscountsQueryHandler : IQueryHandler<EmptyQueryRequestDto, GetAllDiscountsResponseDto>
    {
        private readonly IQueryRepository<Discount> _repository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="GetAllDiscountsQueryHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public GetAllDiscountsQueryHandler(IQueryRepository<Discount> repository,
                                       IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<GetAllDiscountsResponseDto> Handle(EmptyQueryRequestDto model)
        {
            return new GetAllDiscountsResponseDto
            {
                Discounts = _mapper.Map<IReadOnlyCollection<GetAllDiscountsSingleDiscount>>(await _repository.GetAllAsync())
            };
        }
    }
}
