﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllDiscounts
{
    /// <summary>
    /// Модель данных для ответа на запрос по получению всех скидок.
    /// </summary>
    public record GetAllDiscountsResponseDto : IQueryDto
    {
        /// <summary>
        /// Список со всеми скидками.
        /// </summary>
        public IReadOnlyCollection<GetAllDiscountsSingleDiscount> Discounts { get; init; }
    }
}
