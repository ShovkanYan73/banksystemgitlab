﻿namespace BankSystem.Main.Queries.GetAllDiscounts
{
    /// <summary>
    /// Модель данных для одной скидки.
    /// </summary>
    public class GetAllDiscountsSingleDiscount
    {
        /// <summary>
        /// Идентификатор скидки.
        /// </summary>
        public Guid Id { get; internal set; }

        /// <summary>
        /// Наименование скидки.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Размер скидки в процентах.
        /// </summary>
        public decimal DiscountRate { get; internal set; }
    }
}
