﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.Contracts;
using BankSystem.Main.Queries.GetAllBanks;

namespace BankSystem.Main.Queries.GetAllProducts
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class GetAllProductsQueryHandler : IQueryHandler<EmptyQueryRequestDto, GetAllProductsResponseDto>
    {
        private readonly IQueryRepository<Product> _repository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="GetAllProductsQueryHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public GetAllProductsQueryHandler(IQueryRepository<Product> repository,
                                       IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        public async Task<GetAllProductsResponseDto> Handle(EmptyQueryRequestDto model)
        {
            return new GetAllProductsResponseDto
            {
                Products = _mapper.Map<IReadOnlyCollection<GetAllProductsSingleProduct>>(await _repository.GetAllAsync())
            };
        }
    }
}
