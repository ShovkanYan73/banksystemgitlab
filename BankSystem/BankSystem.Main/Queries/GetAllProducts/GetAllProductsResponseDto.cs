﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllProducts
{
    /// <summary>
    /// Модель данных для ответа на запрос по получению всех продуктов.
    /// </summary>
    public record GetAllProductsResponseDto : IQueryDto
    {
        /// <summary>
        /// Список со всеми продуктами.
        /// </summary>
        public IReadOnlyCollection<GetAllProductsSingleProduct> Products { get; init; }
    }
}
