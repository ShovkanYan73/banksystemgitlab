﻿using BankSystem.Database.Models;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllProducts
{
    /// <summary>
    /// Модель данных для одного продукта.
    /// </summary>
    public record GetAllProductsSingleProduct : IQueryDto
    {
        /// <summary>
        /// Идентификатор продукта.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Наименование продукта.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Список субпродуктов.
        /// </summary>
        public virtual List<Subproduct> Subproducts { get; init; }
    }
}
