﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllSubproducts
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class GetAllSubproductsQueryHandler : IQueryHandler<EmptyQueryRequestDto, GetAllSubproductsResponseDto>
    {
        private readonly IQueryRepository<Subproduct> _repository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="GetAllSubproductsQueryHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public GetAllSubproductsQueryHandler(IQueryRepository<Subproduct> repository,
                                       IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<GetAllSubproductsResponseDto> Handle(EmptyQueryRequestDto model)
        {
            return new GetAllSubproductsResponseDto
            {
                Subproducts = _mapper.Map<IReadOnlyCollection<GetAllSubproductsSingleSubproduct>>(await _repository.GetAllAsync())
            };
        }
    }
}
