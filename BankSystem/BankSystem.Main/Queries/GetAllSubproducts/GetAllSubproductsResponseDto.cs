﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllSubproducts
{
    /// <summary>
    /// Модель данных для ответа на запрос по получению всех субпродуктов.
    /// </summary>
    public record GetAllSubproductsResponseDto : IQueryDto
    {
        /// <summary>
        /// Список всех субпродуктов.
        /// </summary>
        public IReadOnlyCollection<GetAllSubproductsSingleSubproduct> Subproducts { get; init; }
    }
}
