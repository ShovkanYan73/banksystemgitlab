﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetAllSubproducts
{
    /// <summary>
    /// Модель данных для одного субпродукта.
    /// </summary>
    public record GetAllSubproductsSingleSubproduct : IQueryDto
    {
        /// <summary>
        /// Идентификатор субпродукта.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Наименование субпродукта.
        /// </summary>
        public string Name { get; init; }
        
        /// <summary>
        /// Процентная ставка субпродукта.
        /// </summary>
        public decimal Rate { get; init; }
    }
}
