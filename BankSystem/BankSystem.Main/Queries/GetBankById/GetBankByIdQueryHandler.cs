﻿using AutoMapper;
using BankSystem.Database.Models;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetBankById
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class GetBankByIdQueryHandler : IQueryHandler<GetBankByIdRequestDto, GetBankByIdResponseDto>
    {
        private readonly IQueryRepository<Bank> _repository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Создание экземпляра объекта типа <see cref="GetBankByIdQueryHandler"/>
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="mapper"></param>
        public GetBankByIdQueryHandler(IQueryRepository<Bank> repository,
                                       IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <param name="model"><inheritdoc/></param>
        /// <returns></returns>
        public async Task<GetBankByIdResponseDto> Handle(GetBankByIdRequestDto model)
        {
            return _mapper.Map<GetBankByIdResponseDto>(await _repository.GetByIdAsync(model.BankId));
        }
    }
}
