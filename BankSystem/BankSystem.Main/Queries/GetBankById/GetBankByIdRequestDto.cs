﻿using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetBankById
{
    /// <summary>
    /// Модель данных для запроса по получению банка по Id.
    /// </summary>
    public record GetBankByIdRequestDto : IQueryDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid BankId { get; init; }
    }
}
