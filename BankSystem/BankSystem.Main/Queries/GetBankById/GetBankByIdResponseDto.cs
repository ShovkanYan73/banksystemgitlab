﻿using BankSystem.Database.Models;
using BankSystem.Main.Contracts;

namespace BankSystem.Main.Queries.GetBankById
{
    /// <summary>
    /// Модель данных для ответа на запрос по получению банка по Id.
    /// </summary>
    public record GetBankByIdResponseDto : IQueryDto
    {
        /// <summary>
        /// Идентификатор банка.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Наименование банка.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Список офисов банка.
        /// </summary>
        public List<Office> Offices { get; init; }

        /// <summary>
        /// Список продукции банка.
        /// </summary>
        public List<Product> Products { get; init; }

        /// <summary>
        /// Список скидок банка.
        /// </summary>
        public List<Discount> Discounts { get; init; }
    }
}
