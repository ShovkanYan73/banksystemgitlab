﻿using Autofac.Extensions.DependencyInjection;
using Autofac;
using BankSystem.Database.Services.Contracts;
using BankSystem.Database.Services;
using BankSystem.Main.Contracts;
using Microsoft.EntityFrameworkCore;
using BankSystem.Database;
using BankSystem.Main.Commands.CreateBank;
using BankSystem.Main.Automapper.Profiles;
using BankSystem.Database.Repositories.Contracts;
using BankSystem.Database.Repositories;
using MediatR;
using BankSystem.Main.EventHandlers.Abstractions;

namespace BankSystem.Main
{
    public partial class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutofac();

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddAutoMapper(typeof(BankProfile).Assembly);

            services.AddDbContext<BankSystemDatabase>(x => x.UseNpgsql(Configuration.GetConnectionString("PostgreSQLConnection")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            
            services.AddScoped(typeof(IReadRepository<>), typeof(ReadRepository<>));
            services.AddScoped(typeof(ICommandRepository<>), typeof(CommandRepository<>));
            services.AddScoped(typeof(IQueryRepository<>), typeof(QueryRepository<>));

            services.AddMediatR(typeof(DomainEventHandler<>).Assembly);   
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(CreateBankCommandHandler).Assembly)
                .Where(x => x.GetInterfaces().Any(_ => _.IsGenericType && (typeof(ICommandHandler<>) == _.GetGenericTypeDefinition() ||
                typeof(IQueryHandler<,>) == _.GetGenericTypeDefinition())))
                .AsImplementedInterfaces();
        }
    }
}
