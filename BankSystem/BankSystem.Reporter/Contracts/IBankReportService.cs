﻿namespace BankSystem.Reporter.Contracts
{
    public interface IBankReportService
    {
        public Task<byte[]> CreateReportAsync();
    }
}
