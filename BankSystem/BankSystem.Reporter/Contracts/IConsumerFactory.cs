﻿using Confluent.Kafka;

namespace BankSystem.Reporter.Contracts
{
    public interface IConsumerFactory
    {
        public IConsumer<TKey, TValue> Create<TKey, TValue>(string name);
    }
}
