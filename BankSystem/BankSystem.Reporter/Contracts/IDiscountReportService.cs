﻿namespace BankSystem.Reporter.Contracts
{
    public interface IDiscountReportService
    {
        public Task<byte[]> CreateReportAsync(Guid bankId);
    }
}
