﻿namespace BankSystem.Reporter.Contracts
{
    public interface IProductReportService
    {
        public Task<byte[]> CreateReportAsync(Guid bankId);
    }
}
