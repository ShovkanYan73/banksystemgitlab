﻿using BankSystem.Reporter.Contracts;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Threading;

namespace BankSystem.Reporter.Controllers
{
    [Route("api/reports")]
    public class ReportController : Controller
    {
        private readonly IBankReportService _bankReportService;
        private readonly IProductReportService _productReportService;
        private readonly IDiscountReportService _discountReportService;
        private const string ContentType = "application/octet-stream";
        private const string BankReportFileName = "bank_report.docx";
        private const string DiscountReportFileName = "discount_report.docx";
        private const string ProductReportFileName = "product_report.docx";

        public ReportController(IBankReportService bankReportService,
                                IProductReportService productReportService,
                                IDiscountReportService discountReportService)
        {
            _bankReportService = bankReportService;
            _productReportService = productReportService;
            _discountReportService = discountReportService;
        }

        [HttpGet("bank")]
        public async Task<FileResult> CreateBankReport()
           => File(await _bankReportService.CreateReportAsync(), ContentType, BankReportFileName);

        [HttpGet("{id}/product")]
        public async Task<FileResult> CreateProductReport(Guid id)
             => File(await _productReportService.CreateReportAsync(id), ContentType, ProductReportFileName);
        [HttpGet("{id}/discount")]
        public async Task<FileResult> CreateDiscountReport(Guid id)
            => File(await _discountReportService.CreateReportAsync(id), ContentType, DiscountReportFileName);
    }
}
