﻿namespace BankSystem.Reporter.Database.Models
{
    public class Bank
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }
    }
}
