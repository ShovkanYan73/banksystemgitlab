﻿namespace BankSystem.Reporter.Database.Models
{
    public class Discount
    {
        public Guid Id { get; set; }


        public string? Name { get; set; }


        public decimal? DiscountRate { get; set; }


        public Guid BankId { get; set; }
    }
}
