﻿namespace BankSystem.Reporter.Database.Models
{
    public class Product
    {
        public Guid Id { get; set; }


        public string? Name { get; set; }


        public Guid BankId { get; set; }
    }
}
