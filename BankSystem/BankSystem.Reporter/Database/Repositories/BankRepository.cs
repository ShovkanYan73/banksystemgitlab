﻿using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using Dapper;
using Microsoft.Data.SqlClient;
using Npgsql;
using System.Data;

namespace BankSystem.Reporter.Database.Repositories
{
    public class BankRepository : IBankRepository
    {
        private readonly IDbConnection _dbConnection;
        private const string Table = "Banks";
        public BankRepository(string connectionString)
        {
            _dbConnection = new NpgsqlConnection(connectionString);
        }

        public async Task<IReadOnlyCollection<Bank>> GetAllAsync()
        {
            return (await _dbConnection.QueryAsync<Bank>($"SELECT * FROM {Table}")).ToArray();
        }

        public async Task<Bank> GetOneAsync(Guid id)
        {
            return await _dbConnection.QueryFirstAsync<Bank>($"SELECT * FROM {Table} WHERE Id = '{id}'");
        }

        public async Task UpdateAsync(Bank model)
        {
            var oldModel = await GetOneAsync(model.Id);
            
            UpdateModel(model, oldModel);
            
            var sqlQuery = $"UPDATE {Table} SET Name = @{nameof(model.Name)} WHERE Id = @{nameof(model.Id)}";
            await _dbConnection.ExecuteAsync(sqlQuery, model);
        }

        public async Task DeleteAsync(DeleteEntityDto model)
        {
            var sqlQuery = $"DELETE FROM {Table} WHERE Id = @{nameof(model.EntityId)}";
            await _dbConnection.ExecuteAsync(sqlQuery, model);
        }

        private void UpdateModel(Bank newModel, Bank oldModel)
        {
            newModel.Name = newModel.Name ?? oldModel.Name;
        }
    }
}
