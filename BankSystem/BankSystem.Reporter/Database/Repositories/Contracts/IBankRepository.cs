﻿using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Models;

namespace BankSystem.Reporter.Database.Repositories.Contracts
{
    public interface IBankRepository
    {
        public Task UpdateAsync(Bank model);

        public Task<IReadOnlyCollection<Bank>> GetAllAsync();
        
        public Task<Bank> GetOneAsync(Guid id);

        public Task DeleteAsync(DeleteEntityDto model);
    }
}
