﻿using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Models;

namespace BankSystem.Reporter.Database.Repositories.Contracts
{
    public interface IDiscountRepository
    {
        public Task UpdateAsync(Discount model);

        public Task<IReadOnlyCollection<Discount>> GetAllAsync();

        public Task<IReadOnlyCollection<Discount>> GetByBankIdAsync(Guid bankId);

        public Task<Discount> GetOneAsync(Guid id);

        public Task DeleteAsync(DeleteEntityDto model);
    }
}
