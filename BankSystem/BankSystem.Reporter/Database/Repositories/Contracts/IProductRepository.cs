﻿using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Models;

namespace BankSystem.Reporter.Database.Repositories.Contracts
{
    public interface IProductRepository
    {
        public Task UpdateAsync(Product model);

        public Task<IReadOnlyCollection<Product>> GetAllAsync();

        public Task<IReadOnlyCollection<Product>> GetByBankIdAsync(Guid bankId);

        public Task<Product> GetOneAsync(Guid id);

        public Task DeleteAsync(DeleteEntityDto model);
    }
}
