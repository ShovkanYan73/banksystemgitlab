﻿using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using Dapper;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.Data.SqlClient;
using Npgsql;
using System.Data;

namespace BankSystem.Reporter.Database.Repositories
{
    public class DiscountRepository : IDiscountRepository
    {
        private readonly IDbConnection _dbConnection;
        private const string Table = "Discounts";
        public DiscountRepository(string connectionString)
        {
            _dbConnection = new NpgsqlConnection(connectionString);
        }

        public async Task<IReadOnlyCollection<Discount>> GetAllAsync()
        {
            return (await _dbConnection.QueryAsync<Discount>($"SELECT * FROM {Table}")).ToArray();
        }

        public async Task<Discount> GetOneAsync(Guid id)
        {
            return await _dbConnection.QueryFirstAsync<Discount>($"SELECT * FROM {Table} WHERE Id = '{id}'");
        }

        public async Task<IReadOnlyCollection<Discount>> GetByBankIdAsync(Guid bankId)
        {
            return (await _dbConnection.QueryAsync<Discount>($"SELECT * FROM {Table} WHERE bankId = '{bankId}'")).ToArray();
        }

        public async Task UpdateAsync(Discount model)
        {
            var oldModel = await GetOneAsync(model.Id);

            UpdateModel(model, oldModel);

            var sqlQuery = $"UPDATE {Table} SET Name = @{nameof(model.Name)}, DiscountRate = @{nameof(model.DiscountRate)} WHERE Id = @{nameof(model.Id)}";
            await _dbConnection.ExecuteAsync(sqlQuery, model);
        }

        public async Task DeleteAsync(DeleteEntityDto model)
        {
            var sqlQuery = $"DELETE FROM {Table} WHERE Id = @{nameof(model.EntityId)}";
            await _dbConnection.ExecuteAsync(sqlQuery, model);
        }

        private void UpdateModel(Discount newModel, Discount oldModel)
        {
            newModel.Name = newModel.Name ?? oldModel.Name;
            newModel.DiscountRate = newModel.DiscountRate ?? oldModel.DiscountRate;
        }
    }
}
