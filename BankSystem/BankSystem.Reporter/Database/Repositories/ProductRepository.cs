﻿using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using Dapper;
using Microsoft.Data.SqlClient;
using Npgsql;
using System.Data;

namespace BankSystem.Reporter.Database.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IDbConnection _dbConnection;
        private const string Table = "Products";

        public ProductRepository(string connectionString)
        {
            _dbConnection = new NpgsqlConnection(connectionString);
        }

        public async Task<IReadOnlyCollection<Product>> GetAllAsync()
        {
            return (await _dbConnection.QueryAsync<Product>($"SELECT * FROM {Table}")).ToArray();
        }

        public async Task<Product> GetOneAsync(Guid id)
        {
            return await _dbConnection.QueryFirstAsync<Product>($"SELECT * FROM {Table} WHERE Id = '{id}'");
        }

        public async Task<IReadOnlyCollection<Product>> GetByBankIdAsync(Guid bankId)
        {
            return (await _dbConnection.QueryAsync<Product>($"SELECT * FROM {Table} WHERE bankId = '{bankId}'")).ToArray();
        }

        public async Task UpdateAsync(Product model)
        {
            var oldModel = await GetOneAsync(model.Id);

            UpdateModel(model, oldModel);

            var sqlQuery = $"UPDATE {Table} SET Name = @{nameof(model.Name)} WHERE Id = @{nameof(model.Id)}";
            await _dbConnection.ExecuteAsync(sqlQuery, model);
        }

        public async Task DeleteAsync(DeleteEntityDto model)
        {
            var sqlQuery = $"DELETE FROM {Table} WHERE Id = @{nameof(model.EntityId)}";
            await _dbConnection.ExecuteAsync(sqlQuery, model);
        }

        private void UpdateModel(Product newModel, Product oldModel)
        {
            newModel.Name = newModel.Name ?? oldModel.Name;
        }
    }
}
