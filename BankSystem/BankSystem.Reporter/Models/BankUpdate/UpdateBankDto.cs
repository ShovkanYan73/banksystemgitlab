﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models.BankUpdate
{
    public record UpdateBankDto
    {
        [JsonProperty]
        public string Name { get; set; }
    }
}
