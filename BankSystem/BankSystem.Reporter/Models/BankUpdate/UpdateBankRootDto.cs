﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models.BankUpdate
{
    public record UpdateBankRootDto
    {
        [JsonProperty]
        public Guid EntityId { get; init; }

        [JsonProperty]
        public UpdateBankDto UpdateBankDto { get; init; }
    }
}
