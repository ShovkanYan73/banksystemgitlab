﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models
{
    public record DeleteEntityDto
    {
        [JsonProperty]
        public Guid EntityId { get; init; }
    }
}
