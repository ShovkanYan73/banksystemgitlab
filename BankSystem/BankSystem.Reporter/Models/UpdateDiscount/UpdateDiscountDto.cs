﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models.UpdateDiscount
{
    public record UpdateDiscountDto
    {
        [JsonProperty]
        public Guid DiscountId { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public decimal DiscountRate { get; set; }
    }
}
