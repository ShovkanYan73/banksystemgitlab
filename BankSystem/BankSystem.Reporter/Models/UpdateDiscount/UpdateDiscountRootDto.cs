﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models.UpdateDiscount
{
    public record UpdateDiscountRootDto
    {
        [JsonProperty]
        public UpdateDiscountDto UpdateDiscountDto { get; set; }
    }
}
