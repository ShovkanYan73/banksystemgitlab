﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models.UpdateProduct
{
    public record UpdateProductDto
    {
        [JsonProperty]
        public Guid ProductId { get; init; }

        [JsonProperty]
        public string Name { get; init; }
    }
}
