﻿using Newtonsoft.Json;

namespace BankSystem.Reporter.Models.UpdateProduct
{
    public record UpdateProductRootDto
    {
        [JsonProperty]
        public UpdateProductDto UpdateProductDto { get; init; }
    }
}
