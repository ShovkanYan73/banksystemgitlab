﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Reflection;

namespace BankSystem.Reporter.Services
{
    public class BankDeletedConsumerService
    {
        private readonly IBankRepository _bankRepository;
        private readonly IConsumer<string, string> _consumer;
        private readonly string _topic;

        public BankDeletedConsumerService(IBankRepository bankRepository,
                                          IConsumerFactory consumerFactory,
                                          string topic)
        {
            _bankRepository = bankRepository;
            _consumer = consumerFactory.Create<string, string>(topic);
            _topic = topic;
        }

        public async Task StartAsync()
        {
            _consumer.Subscribe(_topic);

            while (true)
            {
                try
                {
                    var result = await Task.Run(() => _consumer.Consume());
                    var model = JsonConvert.DeserializeObject<DeleteEntityDto>(result.Message.Value);
                    await _bankRepository.DeleteAsync(model);
                }
                catch
                {
                    throw new Exception("Данного топика не существует.");
                }
            }
        }
    }
}
