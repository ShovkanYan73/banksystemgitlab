﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories.Contracts;
using TemplateEngine.Docx;

namespace BankSystem.Reporter.Services
{
    public class BankReportService : IBankReportService
    {
        private readonly IBankRepository _bankRepository;

        public BankReportService(IBankRepository bankRepository)
        {
            _bankRepository = bankRepository;
        }

        public async Task<byte[]> CreateReportAsync()
        {
            string templateFilePath = "bank_template.docx";
            string reportFilePath = "bank_report.docx";

            if (File.Exists(reportFilePath))
            {
                File.Delete(reportFilePath);
            }
            File.Copy(templateFilePath, reportFilePath);

            var banks = await _bankRepository.GetAllAsync();

            var table = new TableContent("Banks table");

            foreach (var bank in banks)
            {
                table.AddRow(
                    new FieldContent("Bank Id", bank.Id.ToString()),
                    new FieldContent("Bank name", bank.Name));
            }

            var valuesToFill = new Content(table);

            using (var outputDocument = new TemplateProcessor(reportFilePath)
                .SetRemoveContentControls(true))
            {
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }

            return File.ReadAllBytes(reportFilePath);
        }
    }
}
