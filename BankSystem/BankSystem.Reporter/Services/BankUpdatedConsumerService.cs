﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using BankSystem.Reporter.Models.BankUpdate;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace BankSystem.Reporter.Services
{
    public class BankUpdatedConsumerService
    {
        private readonly IBankRepository _bankRepository;
        private readonly IConsumer<string, string> _consumer;
        private readonly string _topic;

        public BankUpdatedConsumerService(IBankRepository bankRepository,
                                                IConsumerFactory consumerFactory,
                                                string topic)
        {
            _bankRepository = bankRepository;
            _consumer = consumerFactory.Create<string, string>(topic);
            _topic = topic;
        }

        public async Task StartAsync()
        {
            _consumer.Subscribe(_topic);

            while (true)
            {
                try
                {
                    var result = await Task.Run(() => _consumer.Consume());
                    
                    var model = JsonConvert.DeserializeObject<UpdateBankRootDto>(result.Message.Value);

                    if(model == null)
                    {
                        continue;
                    }

                    await _bankRepository.UpdateAsync(new Bank {
                        Id = model.EntityId,
                        Name = model.UpdateBankDto.Name
                    });
                }
                catch
                {
                    throw new Exception("Данного топика не существует.");
                }
            }
        }
    }
}
