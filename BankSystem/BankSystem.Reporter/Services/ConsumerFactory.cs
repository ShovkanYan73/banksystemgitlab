﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Settings;
using Confluent.Kafka;
using Microsoft.Extensions.Options;

namespace BankSystem.Reporter.Services
{
    public class ConsumerFactory : IConsumerFactory
    {
        private readonly ConsumerListSettings _options;

        public ConsumerFactory(IOptions<ConsumerListSettings> options)
        {
            _options = options.Value;
        }

        public IConsumer<TKey, TValue> Create<TKey, TValue>(string topic)
        {
            return new ConsumerBuilder<TKey, TValue>(new ConsumerConfig
            {
                BootstrapServers = _options.Consumers.First(x => x.Topic.Equals(topic)).BootstrapServers,
                GroupId = _options.Consumers.First(x => x.Topic.Equals(topic)).GroupId,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                EnableAutoCommit = true,
                SessionTimeoutMs = 10000,
                HeartbeatIntervalMs = 600
            }).Build();
        }
    }
}
