﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace BankSystem.Reporter.Services
{
    public class DiscountDeletedConsumerService
    {
        private readonly IDiscountRepository _discountRepository;
        private readonly IConsumer<string, string> _consumer;
        private readonly string _topic;

        public DiscountDeletedConsumerService(IDiscountRepository discountRepository,
                                                    IConsumerFactory consumerFactory,
                                                    string topic)
        {
            _discountRepository = discountRepository;
            _consumer = consumerFactory.Create<string, string>(topic);
            _topic = topic;
        }

        public async Task StartAsync()
        {
            _consumer.Subscribe(_topic);

            while (true)
            {
                try
                {
                    var result = await Task.Run(() => _consumer.Consume());
                    var model = JsonConvert.DeserializeObject<DeleteEntityDto>(result.Message.Value);
                    await _discountRepository.DeleteAsync(model);
                }
                catch
                {
                    throw new Exception("Данного топика не существует.");
                }
            }
        }

    }
}
