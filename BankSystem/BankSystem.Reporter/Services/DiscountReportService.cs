﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories.Contracts;
using TemplateEngine.Docx;

namespace BankSystem.Reporter.Services
{
    public class DiscountReportService : IDiscountReportService
    {
        private readonly IDiscountRepository _discountRepository;
        private readonly IBankRepository _bankRepository;

        public DiscountReportService(IDiscountRepository discountRepository,
                                     IBankRepository bankRepository)
        {
            _discountRepository = discountRepository;
            _bankRepository = bankRepository;
        }

        public async Task<byte[]> CreateReportAsync(Guid bankId)
        {
            string templateFilePath = "discount_template.docx";
            string reportFilePath = "discount_report.docx";

            if (File.Exists(reportFilePath))
            {
                File.Delete(reportFilePath);
            }
            File.Copy(templateFilePath, reportFilePath);

            var discounts = await _discountRepository.GetByBankIdAsync(bankId);
            var bank = await _bankRepository.GetOneAsync(bankId);

            var bankName = new FieldContent("Bank name", bank.Name);

            var table = new TableContent("Discounts table");

            foreach (var discount in discounts)
            {
                table.AddRow(
                    new FieldContent("Discount Id", discount.Id.ToString()),
                    new FieldContent("Discount name", discount.Name),
                    new FieldContent("Discount rate", discount.DiscountRate.ToString()));
            }

            var valuesToFill = new Content(bankName, table);

            using (var outputDocument = new TemplateProcessor(reportFilePath)
                .SetRemoveContentControls(true))
            {
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }

            return File.ReadAllBytes(reportFilePath);
        }
    }
}
