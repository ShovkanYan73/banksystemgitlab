﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models.UpdateDiscount;
using BankSystem.Reporter.Models.UpdateProduct;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace BankSystem.Reporter.Services
{
    public class DiscountUpdatedConsumerService
    {
        private readonly IDiscountRepository _discountRepository;
        private readonly IConsumer<string, string> _consumer;
        private readonly string _topic;

        public DiscountUpdatedConsumerService(IDiscountRepository discountRepository,
                                                    IConsumerFactory consumerFactory,
                                                    string topic)
        {
            _discountRepository = discountRepository;
            _consumer = consumerFactory.Create<string, string>(topic);
            _topic = topic;
        }

        public async Task StartAsync()
        {
            _consumer.Subscribe(_topic);

            while (true)
            {
                try
                {
                    var result = await Task.Run(() => _consumer.Consume());

                    var model = JsonConvert.DeserializeObject<UpdateDiscountRootDto>(result.Message.Value);

                    if (model == null)
                    {
                        continue;
                    }

                    await _discountRepository.UpdateAsync(new Discount
                    {
                        Id = model.UpdateDiscountDto.DiscountId,
                        Name = model.UpdateDiscountDto.Name,
                        DiscountRate = model.UpdateDiscountDto.DiscountRate
                    });
                }
                catch
                {
                    throw new Exception("Данного топика не существует.");
                }
            }
        }
    }
}
