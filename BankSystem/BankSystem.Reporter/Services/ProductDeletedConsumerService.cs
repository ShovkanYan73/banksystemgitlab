﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace BankSystem.Reporter.Services
{
    public class ProductDeletedConsumerService
    {
        private readonly IProductRepository _productRepository;
        private readonly IConsumer<string, string> _consumer;
        private readonly string _topic;

        public ProductDeletedConsumerService(IProductRepository productRepository,
                                                   IConsumerFactory consumerFactory,
                                                   string topic)
        {
            _productRepository = productRepository;
            _consumer = consumerFactory.Create<string, string>(topic);
            _topic = topic;
        }

        public async Task StartAsync()
        {
            _consumer.Subscribe(_topic);

            while (true)
            {
                try
                {
                    var result = await Task.Run(() => _consumer.Consume());
                    var model = JsonConvert.DeserializeObject<DeleteEntityDto>(result.Message.Value);
                    await _productRepository.DeleteAsync(model);
                }
                catch
                {
                    throw new Exception("Данного топика не существует.");
                }
            }
        }
    }
}
