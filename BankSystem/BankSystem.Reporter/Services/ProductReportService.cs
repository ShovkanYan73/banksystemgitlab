﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories;
using BankSystem.Reporter.Database.Repositories.Contracts;
using TemplateEngine.Docx;

namespace BankSystem.Reporter.Services
{
    public class ProductReportService : IProductReportService
    {
        private readonly IProductRepository _productRepository;
        private readonly IBankRepository _bankRepository;

        public ProductReportService(IProductRepository productRepository,
                                    IBankRepository bankRepository)
        {
            _productRepository = productRepository;
            _bankRepository = bankRepository;
        }

        public async Task<byte[]> CreateReportAsync(Guid bankId)
        {
            string templateFilePath = "product_template.docx";
            string reportFilePath = "product_report.docx";

            if (File.Exists(reportFilePath))
            {
                File.Delete(reportFilePath);
            }
            File.Copy(templateFilePath, reportFilePath);

            var products = await _productRepository.GetByBankIdAsync(bankId);
            var bank = await _bankRepository.GetOneAsync(bankId);

            var bankName = new FieldContent("Bank name", bank.Name);

            var table = new TableContent("Products table");

            foreach (var product in products)
            {
                table.AddRow(
                    new FieldContent("Product Id", product.Id.ToString()),
                    new FieldContent("Product name", product.Name));
            }

            var valuesToFill = new Content(bankName, table);

            using (var outputDocument = new TemplateProcessor(reportFilePath)
                .SetRemoveContentControls(true))
            {
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }

            return File.ReadAllBytes(reportFilePath);
        }
    }
}
