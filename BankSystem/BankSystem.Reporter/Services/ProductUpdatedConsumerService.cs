﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Models;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Models.BankUpdate;
using BankSystem.Reporter.Models.UpdateProduct;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace BankSystem.Reporter.Services
{
    public class ProductUpdatedConsumerService
    {
        private readonly IProductRepository _productRepository;
        private readonly IConsumer<string, string> _consumer;
        private readonly string _topic;

        public ProductUpdatedConsumerService(IProductRepository productRepository,
                                                IConsumerFactory consumerFactory,
                                                string topic)
        {
            _productRepository = productRepository;
            _consumer = consumerFactory.Create<string, string>(topic);
            _topic = topic;
        }

        public async Task StartAsync()
        {
            _consumer.Subscribe(_topic);

            while (true)
            {
                try
                {
                    var result = await Task.Run(() => _consumer.Consume());

                    var model = JsonConvert.DeserializeObject<UpdateProductRootDto>(result.Message.Value);

                    if (model == null)
                    {
                        continue;
                    }

                    await _productRepository.UpdateAsync(new Product
                    {
                        Id = model.UpdateProductDto.ProductId,
                        Name = model.UpdateProductDto.Name
                    });
                }
                catch
                {
                    throw new Exception("Данного топика не существует.");
                }
            }
        }
    }
}
