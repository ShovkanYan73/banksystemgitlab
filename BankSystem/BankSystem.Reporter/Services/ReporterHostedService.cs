﻿namespace BankSystem.Reporter.Services
{
    public class ReporterHostedService : IHostedService
    {
        private readonly BankDeletedConsumerService _bankDeletedConsumerService;
        private readonly BankUpdatedConsumerService _bankUpdatedConsumerService;
        private readonly DiscountDeletedConsumerService _discountDeletedConsumerService;
        private readonly DiscountUpdatedConsumerService _discountUpdatedConsumerService;
        private readonly ProductDeletedConsumerService _productDeletedConsumerService;
        private readonly ProductUpdatedConsumerService _productUpdatedConsumerService;

        public ReporterHostedService(BankDeletedConsumerService bankDeletedConsumerService,
                                     BankUpdatedConsumerService bankUpdatedConsumerService,
                                     DiscountDeletedConsumerService discountDeletedConsumerService, 
                                     DiscountUpdatedConsumerService discountUpdatedConsumerService,
                                     ProductDeletedConsumerService productDeletedConsumerService,
                                     ProductUpdatedConsumerService productUpdatedConsumerService)
        {
            _bankDeletedConsumerService = bankDeletedConsumerService;
            _bankUpdatedConsumerService = bankUpdatedConsumerService;
            _discountDeletedConsumerService = discountDeletedConsumerService;
            _discountUpdatedConsumerService = discountUpdatedConsumerService;
            _productDeletedConsumerService = productDeletedConsumerService;
            _productUpdatedConsumerService = productUpdatedConsumerService;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(() =>
            {
                _bankDeletedConsumerService.StartAsync();
                _bankUpdatedConsumerService.StartAsync();
                _discountDeletedConsumerService.StartAsync();
                _discountUpdatedConsumerService.StartAsync();
                _productDeletedConsumerService.StartAsync();
                _productUpdatedConsumerService.StartAsync();
            });

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
