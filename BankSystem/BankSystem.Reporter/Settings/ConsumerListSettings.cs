﻿namespace BankSystem.Reporter.Settings
{
    public record ConsumerListSettings
    {
        public IReadOnlyCollection<ConsumerSettings> Consumers { get; init; }
    }
}
