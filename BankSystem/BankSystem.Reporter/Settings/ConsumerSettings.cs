﻿namespace BankSystem.Reporter.Settings
{
    public record ConsumerSettings
    {
        public string Topic { get; init; }

        public string BootstrapServers { get; set; }

        public string GroupId { get; set; }
    }
}
