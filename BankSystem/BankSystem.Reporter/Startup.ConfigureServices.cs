﻿using BankSystem.Reporter.Contracts;
using BankSystem.Reporter.Database.Repositories;
using BankSystem.Reporter.Database.Repositories.Contracts;
using BankSystem.Reporter.Services;
using BankSystem.Reporter.Settings;
using Microsoft.Extensions.DependencyInjection;

namespace BankSystem.Reporter
{
    public partial class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();


            services.AddScoped<IConsumerFactory, ConsumerFactory>();

            services.AddScoped<IBankRepository, BankRepository>(provider => new BankRepository(Configuration.GetConnectionString("PostgreSQLConnection")));
            services.AddScoped<IDiscountRepository, DiscountRepository>(provider => new DiscountRepository(Configuration.GetConnectionString("PostgreSQLConnection")));
            services.AddScoped<IProductRepository, ProductRepository>(provider => new ProductRepository(Configuration.GetConnectionString("PostgreSQLConnection")));

            services.Configure<ConsumerListSettings>(Configuration.GetSection(nameof(ConsumerListSettings)));

            services.AddSingleton(provider => new BankDeletedConsumerService(
                        services.BuildServiceProvider().GetRequiredService<IBankRepository>(),
                        services.BuildServiceProvider().GetRequiredService<IConsumerFactory>(),
                        "BankDeletedIntegrationEvent"
                        ));

            services.AddSingleton(provider => new BankUpdatedConsumerService(
                        services.BuildServiceProvider().GetRequiredService<IBankRepository>(),
                        services.BuildServiceProvider().GetRequiredService<IConsumerFactory>(),
                        "BankUpdatedIntegrationEvent"
                        ));

            services.AddSingleton(provider => new ProductDeletedConsumerService(
                        services.BuildServiceProvider().GetRequiredService<IProductRepository>(),
                        services.BuildServiceProvider().GetRequiredService<IConsumerFactory>(),
                        "ProductDeletedIntegrationEvent"
                        ));

            services.AddSingleton(provider => new ProductUpdatedConsumerService(
                        services.BuildServiceProvider().GetRequiredService<IProductRepository>(),
                        services.BuildServiceProvider().GetRequiredService<IConsumerFactory>(),
                        "ProductUpdatedIntegrationEvent"
                        ));

            services.AddSingleton(provider => new DiscountDeletedConsumerService(
                        services.BuildServiceProvider().GetRequiredService<IDiscountRepository>(),
                        services.BuildServiceProvider().GetRequiredService<IConsumerFactory>(),
                        "DiscountDeletedIntegrationEvent"
                        ));

            services.AddSingleton(provider => new DiscountUpdatedConsumerService(
                        services.BuildServiceProvider().GetRequiredService<IDiscountRepository>(),
                        services.BuildServiceProvider().GetRequiredService<IConsumerFactory>(),
                        "DiscountUpdatedIntegrationEvent"
                        ));

            services.AddHostedService<ReporterHostedService>();

            services.AddScoped<IBankReportService, BankReportService>();
            services.AddScoped<IProductReportService, ProductReportService>();
            services.AddScoped<IDiscountReportService, DiscountReportService>();
        }
    }
}
