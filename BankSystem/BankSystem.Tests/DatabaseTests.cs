﻿using AutoFixture;
using BankSystem.Database;
using BankSystem.Database.Helpers.Dto;
using BankSystem.Database.Models;
using BankSystem.Database.Services;
using DocumentFormat.OpenXml.VariantTypes;
using MediatR;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BankSystem.Tests
{
    public class DatabaseTests
    {
        private readonly IFixture _fixture = new Fixture();
        [Fact]
        public void UpdateBank_EventListMustNotBeEmpty()
        {
            // Arrange
            var bank = new Bank(_fixture.Create<string>());

            // Act
            bank.UpdateBank(new UpdateBankDto());

            // Assert
            Assert.NotEmpty(bank.Events);    
        }

        [Fact]
        public void DeleteBank_EventListMustNotBeEmptyAndIsDeletedMustBeTrue()
        {
            // Arrange
            var bank = new Bank(_fixture.Create<string>());


            // Act
            bank.DeleteBank();

            // Assert
            Assert.NotEmpty(bank.Events);
            Assert.True(bank.IsDeleted);
        }
    }
}
