﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeTypes.Specifications
{
    public class AndSpecification<T> : Specification<T>
    {
        public AndSpecification(Specification<T> left, Specification<T> right)
            : base(Expression.Lambda<Func<T, bool>>(Expression.AndAlso(left.Predicate.Body, right.Predicate.Body), left.Predicate.Parameters.Single())) { }
    }

}
